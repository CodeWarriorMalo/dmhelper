﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DMHelper5e.ViewModels;
using DMHelper5e.Views;
using DnD5eData.Models;
namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for CreatureEditorWindow.xaml
    /// </summary>
    public partial class CreatureEditorWindow : Window
    {
        private CreatureEditorView _View;
        public CreatureEditorView View
        {
            get { return _View;}
            set { _View = value; _View.OnClose += _View_OnClose; }
        }
        public bool Saved;
        private void _View_OnClose(bool saving)
        {
            Saved = saving;
            Close();
        }

        public CreatureEditorWindow(Window owner, CreatureModel model =  null, bool isNew = true)
        {
            InitializeComponent();
            this.Owner = owner;
            if(model == null)
                View = new CreatureEditorView(new CreatureEditorViewModel(), this, isNew);
            else
                View = new CreatureEditorView(new CreatureEditorViewModel(model), this, isNew);
            mainGrid.Children.Add(_View);
        }
    }
}
