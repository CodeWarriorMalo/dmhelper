﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DnD5eData.Models;
using DMHelper5e.ViewModels;
using DnD5eData.DataAccess;
using DnD5eData.DataAccess.Requests;
using DnD5eData.DataAccess.Responses;
namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for CreatureDatabaseWindow.xaml
    /// </summary>
    public partial class CreatureDatabaseWindow : Window
    {
        private CreatureDatabaseViewModel _ViewModel;
        public CreatureDatabaseWindow(CreatureDatabaseViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        }
        public CreatureDatabaseViewModel ViewModel
        {
            get
            {
                return _ViewModel;
            }
            set
            {
                _ViewModel = value;
                DataContext = _ViewModel;
            }
        }
        private void lstCreatures_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox listBox = (ListBox)sender;
            CreatureDatabaseViewModel viewModel = CreatureDatabaseViewModel.CreateNew(_ViewModel);
            if (listBox.SelectedIndex > -1)
                viewModel.SelectedCreature = viewModel.Creatures[listBox.SelectedIndex];
            else
                viewModel.SelectedCreature = null; 
            ViewModel = viewModel;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            CreatureEditorWindow editor = new CreatureEditorWindow(this, CreatureModel.CreateNew(), true);
            editor.ShowDialog();
            if(editor.Saved)
            {
                CreatureDatabaseViewModel viewModel = CreatureDatabaseViewModel.CreateNew(_ViewModel);
                viewModel.Creatures.Add(editor.View.ViewModel.Model);
                ViewModel = viewModel;
                
            }
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            CreatureModel model = _ViewModel.SelectedCreature;
            CreatureEditorWindow editor = new CreatureEditorWindow(this, _ViewModel.SelectedCreature, false);
            editor.ShowDialog();
            if (editor.Saved)
            {
                CreatureDatabaseViewModel viewModel = CreatureDatabaseViewModel.CreateNew(_ViewModel);
                viewModel.Creatures.RemoveAll(p => p.Id == model.Id);
                viewModel.Creatures.Add(editor.View.ViewModel.Model);
                ViewModel = viewModel;
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            // prompt then delete on affirmative
            DeleteMonsterRequest request = DeleteMonsterRequest.CreateNew();
            request.Requests.Add(_ViewModel.Creatures[lstCreatures.SelectedIndex]);
            DeleteMonsterResponse response = DatabaseAccess.Instance.DeleteMonster(request);

            CreatureDatabaseViewModel viewModel = CreatureDatabaseViewModel.CreateNew(_ViewModel);
            viewModel.SelectedCreature = null;
            viewModel.Creatures.RemoveAt(lstCreatures.SelectedIndex);
            ViewModel = viewModel;

        }
    }
}
