﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for AlertChangesDialog.xaml
    /// </summary>
    public partial class AlertChangesDialog : Window
    {
        public AlertChangesDialog()
        {
            InitializeComponent();
        }
        public bool Affirmative;
        private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            Affirmative = true;
            Close();
        }

        private void btnNo_Click(object sender, RoutedEventArgs e)
        {
            Affirmative = false;
            Close();
        }
    }
}
