﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DMHelper5e.ViewModels;
namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for AddCreatureWindow.xaml
    /// </summary>
    public partial class AddCreatureWindow : Window
    {
        public AddCreatureViewModel ViewModel = new AddCreatureViewModel();
        
        public AddCreatureWindow(Window owner)
        {
            InitializeComponent();
            this.Owner = owner;
            DataContext = ViewModel;
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.DialogResult = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.DialogResult = false;
            Close();
        }
    }
}
