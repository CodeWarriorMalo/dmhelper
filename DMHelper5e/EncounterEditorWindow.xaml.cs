﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for EncounterEditorWindow.xaml
    /// </summary>
    public partial class EncounterEditorWindow : Window
    {
        public EncounterEditorWindow(string name, string description)
        {
            InitializeComponent();
            EncounterName = name;
            Description = description;
        }
        public bool Saved { get; set; }
        public string EncounterName { get { return txtName.Text; } set { txtName.Text = value; } }
        public string Description { get { return txtDescription.Text; } set { txtDescription.Text = value; } }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Saved = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Saved = false;
            Close();
        }
    }
}
