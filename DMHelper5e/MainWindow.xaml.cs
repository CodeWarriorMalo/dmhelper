﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMHelper5e.ViewModels;
using DMHelper5e.Views;
using DnD5eData.Tools;
using DnD5eData.Models;
using DnD5eData.DataAccess;
using DnD5eData.DataAccess.Requests;
using DnD5eData.DataAccess.Responses;
using System.IO;
using Microsoft.Win32;
namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private CreatureControl _CreatureControl;
        private EncounterControl _EncounterControl;
        private EncounterModel _Encounter;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CreatureEditorWindow editor = new CreatureEditorWindow(this);
            _CreatureControl = new CreatureControl(editor.View.ViewModel, this);
            creatureGrid.Children.Add(_CreatureControl);
            //   editor.Show();

            LoadEncounter();
        }

        private void _EncounterControl_SelectedEncounterItemChanged(string name)
        {
            if (_Encounter.Creatures.Any(c => c.Name == name))
            {
                CreatureModel model = _Encounter.Creatures.First(c => c.Name == name);
                _CreatureControl = new Views.CreatureControl(new ViewModels.CreatureEditorViewModel(model), this);
                creatureGrid.Children.Clear();
                creatureGrid.Children.Add(_CreatureControl);
            }
        }

        private void LoadEncounter()
        {
            if (string.IsNullOrWhiteSpace(Properties.Settings.Default.LastEncounter))
                _Encounter = EncounterModel.CreateNew();
            else
                _Encounter = EncounterModel.Load(Properties.Settings.Default.LastEncounter);

            _EncounterControl = new EncounterControl(_Encounter);
            _EncounterControl.SelectedEncounterItemChanged += _EncounterControl_SelectedEncounterItemChanged;
            encounterGrid.Children.Add(_EncounterControl);
            txtEncounterName.Text = _Encounter.Name;

        }

        private void mnuAllyList_Click(object sender, RoutedEventArgs e)
        {
            // open list editor
        }

        private void mnuNeutralList_Click(object sender, RoutedEventArgs e)
        {
            // open list editor
        }

        private void mnuEnemyList_Click(object sender, RoutedEventArgs e)
        {
            // open list editor
        }

        private void mnuExit_Click(object sender, RoutedEventArgs e)
        {
            // confirm, then
            this.Close();
        }

        private void mnuNewEncounter_Click(object sender, RoutedEventArgs e)
        {
            bool open = true;
            // confirm then open encounter from disk
            if (!_Encounter.HasBeenSaved)
            {
                AlertChangesDialog acd = new AlertChangesDialog();
                acd.Owner = this;
                acd.ShowDialog();
                open = acd.Affirmative;
            }
            if (open)
            {
                _Encounter = EncounterModel.CreateNew();
                EncounterEditorWindow editor = new EncounterEditorWindow(string.Empty, string.Empty);
                editor.Owner = this;
                editor.ShowDialog();
                if(editor.Saved)
                {
                    _Encounter.Description = editor.Description;
                    _Encounter.Name = editor.EncounterName;
                    txtEncounterName.Text = _Encounter.Name;
                }
            }
        }

        private void mnuOpenEncounter_Click(object sender, RoutedEventArgs e)
        {
            bool open = true;
            // confirm then open encounter from disk
            if (!_Encounter.HasBeenSaved)
            {
                AlertChangesDialog acd = new AlertChangesDialog();
                acd.Owner = this;
                acd.ShowDialog();
                open = acd.Affirmative;
            }
            if (open)
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "DM Helper Encounter Files (*.dm5)|*.dm5";
                if (ofd.ShowDialog().Value)
                    _Encounter = EncounterModel.Load(ofd.FileName);
            }

        }

        private void mnuSaveEncounter_Click(object sender, RoutedEventArgs e)
        {
            // save
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "DM Helper Encounter Files (*.dm5)|*.dm5";
            if (sfd.ShowDialog().Value)
                _Encounter.Save(sfd.FileName);
        }

        private void mnuActionInit_Click(object sender, RoutedEventArgs e)
        {
            _EncounterControl.StartInitiative();
        }

        private void mnuActionNext_Click(object sender, RoutedEventArgs e)
        {
            _EncounterControl.SelectNextCreature();
        }

        private void mnuActionPrev_Click(object sender, RoutedEventArgs e)
        {
            _EncounterControl.SelectPreviousCreature();
        }

        private void mnuTakeDamage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var creature = _Encounter.Creatures.First(c => c.Name == _EncounterControl.SelectedCreatureName);
                TakeDamageDialog tdd = new TakeDamageDialog();
                tdd.Owner = this;
                tdd.ShowDialog();
                if (tdd.Affirmative)
                {
                    creature.TakeDamage(tdd.Damage);
                }
            }
            catch
            {
            }
        }

        private void mnuTempHP_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var creature = _Encounter.Creatures.First(c => c.Name == _EncounterControl.SelectedCreatureName);
                TempHitPointsDialog thpd = new TempHitPointsDialog();
                thpd.Owner = this;
                thpd.TempHitPoints = creature.TemporaryHitPoints;
                thpd.ShowDialog();
                if (thpd.Affirmative)
                {
                    creature.TemporaryHitPoints = thpd.TempHitPoints;
                }
            }
            catch
            {
            }
        }

        private void mnuEditCreature_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var creature = _Encounter.Creatures.First(c => c.Name == _EncounterControl.SelectedCreatureName);
                CreatureEditorWindow editor = new CreatureEditorWindow(this, creature, false);
                editor.ShowDialog();
                if (editor.Saved) creature = editor.View.ViewModel.Model;
            }
            catch
            {
            }
        }

        private void mnuAddCreature_Click(object sender, RoutedEventArgs e)
        {
            AddCreatureWindow addCreature = new AddCreatureWindow(this);
            addCreature.ShowDialog();
        }
        private void mnuEndRound_Click(object sender, RoutedEventArgs e)
        {
            _EncounterControl.StartNextRound();
        }

        private void mnuActivate_Click(object sender, RoutedEventArgs e)
        {
            _EncounterControl.ActivateSelectedCreature();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.S && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control) mnuSaveEncounter_Click(this, new RoutedEventArgs());
            else if (e.Key == Key.O && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control) mnuOpenEncounter_Click(this, new RoutedEventArgs());
            else if (e.Key == Key.N && (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control) mnuNewEncounter_Click(this, new RoutedEventArgs());

            else if (e.Key == Key.I) mnuActionInit_Click(this, new RoutedEventArgs());
            else if (e.Key == Key.N) mnuActionNext_Click(this, new RoutedEventArgs());
            else if (e.Key == Key.P) mnuActionPrev_Click(this, new RoutedEventArgs());
            else if (e.Key == Key.F1) // select
            {
                _EncounterControl.SelectActiveCreature();
                _EncounterControl_SelectedEncounterItemChanged(_EncounterControl.SelectedCreatureName);
            }
            else if (e.Key == Key.F2) mnuEditCreature_Click(this, new RoutedEventArgs());
            else if (e.Key == Key.F3) mnuTakeDamage_Click(this, new RoutedEventArgs());
            else if (e.Key == Key.F5) _EncounterControl.ActivateSelectedCreature();
            else if (e.Key == Key.F7) _EncounterControl.ReviveActiveCreature();
            else if (e.Key == Key.F8) _EncounterControl.KillActiveCreature();

            else if (e.Key == Key.F12) mnuEndRound_Click(this, new RoutedEventArgs());
            else if (e.Key == Key.OemPlus) mnuAddCreature_Click(this, new RoutedEventArgs());

        }

        private void mnuCreatureData_Click(object sender, RoutedEventArgs e)
        {
            RetrieveMonsterRequest request = RetrieveMonsterRequest.CreateNew();
            RetrieveMonsterResponse response = DatabaseAccess.Instance.RetrieveMonster(request);
            CreatureDatabaseViewModel viewModel = new CreatureDatabaseViewModel();
            viewModel.Creatures = response.Monsters;
            CreatureDatabaseWindow window = new CreatureDatabaseWindow(viewModel);
            window.Owner = this;
            window.ShowDialog();

        }
    }
}