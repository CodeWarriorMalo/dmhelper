﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DnD5eData.Tools;
using DMHelper5e.ViewModels;
using DMHelper5e.Views;
namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for ConditionTypeEditorWindow.xaml
    /// </summary>
    public partial class ConditionTypeEditorWindow : Window
    {
        private ConditionTypeEditorView _View;
        public List<Enumerations.Conditions> List { get { return _View.List; } }
        public ConditionTypeEditorWindow(List<Enumerations.Conditions> list, Enumerations.Editors editor)
        {
            InitializeComponent();
            _View = new ConditionTypeEditorView(list);
            _View.OnClose += _View_OnClose;
            mainGrid.Children.Add(_View);
            this.Title = string.Format("Condition {0} Editor", editor);
        }
        public bool Saved;
        private void _View_OnClose(bool saving)
        {
            Saved = saving;
            this.Close();
        }
    }
}
