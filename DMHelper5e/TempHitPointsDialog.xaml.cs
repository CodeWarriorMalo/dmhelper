﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for TempHitPointsDialog.xaml
    /// </summary>
    public partial class TempHitPointsDialog : Window
    {
        public TempHitPointsDialog()
        {
            InitializeComponent();
        }
        public bool Affirmative;
        private int _TempHitPoints;
        public int TempHitPoints
        {
            get
            {
                int.TryParse(txtTempHitPoints.Text, out _TempHitPoints);
                return _TempHitPoints;
            }
            set
            {
                _TempHitPoints = value;
                txtTempHitPoints.Text = _TempHitPoints.ToString();
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Affirmative = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Affirmative = false;
            Close();
        }
    }
}
