﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DnD5eData.Tools;
using DnD5eData.Models;
using DMHelper5e.Views;
namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for LanguageEditorWindow.xaml
    /// </summary>
    public partial class LanguageEditorWindow : Window
    {
        private LanguageEditorView _View;
        public List<LanguageModel> List { get { return _View.List; } }
        public LanguageEditorWindow(List<LanguageModel> list)
        {
            InitializeComponent();
            _View = new LanguageEditorView(list);
            _View.OnClose += _View_OnClose;
            mainGrid.Children.Add(_View);
        }
        public bool Saved;
        private void _View_OnClose(bool saving)
        {
            Saved = saving;
            this.Close();
        }
    }
}
