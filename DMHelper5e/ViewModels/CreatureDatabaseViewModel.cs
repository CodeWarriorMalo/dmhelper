﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
namespace DMHelper5e.ViewModels
{
    public class CreatureDatabaseViewModel
    {
        public List<CreatureModel> Creatures { get; set; }
        public CreatureModel SelectedCreature { get; set; }
        public bool EditButtonEnabled { get { return SelectedCreature != null; } }
        public bool DeleteButtonEnabled { get { return SelectedCreature != null; } }
        public static CreatureDatabaseViewModel CreateNew()
        {
            CreatureDatabaseViewModel model = new CreatureDatabaseViewModel();
            model.Creatures = new List<CreatureModel>();
            return model;
        }
        public static CreatureDatabaseViewModel CreateNew(CreatureDatabaseViewModel copy)
        {
            CreatureDatabaseViewModel model = new CreatureDatabaseViewModel();
            model.Creatures = new List<CreatureModel>();
            model.Creatures.AddRange(copy.Creatures);
            model.SelectedCreature = copy.SelectedCreature;
            return model;
        }
    }
}
