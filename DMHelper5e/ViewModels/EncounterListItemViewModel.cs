﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
using DnD5eData.Tools;
using System.ComponentModel;
using System.Windows.Media;
namespace DMHelper5e.ViewModels
{
    public class EncounterListItemViewModel : INotifyPropertyChanged
    {
        private EncounterItemModel _Model;

        public event PropertyChangedEventHandler PropertyChanged;

        public EncounterItemModel Model
        {
            get
            {
                return _Model;
            }
            set
            {
                _Model = value;
            }
        }
        public EncounterListItemViewModel(EncounterItemModel model)
        {
            Model = model;
           
        }
        public void Select()
        {
            _Selected = true;
            PropertyChanged(this, new PropertyChangedEventArgs("SelectedStroke"));
        }
        public void Deselect()
        {
            _Selected = false;
            PropertyChanged(this, new PropertyChangedEventArgs("SelectedStroke"));
        }
        public bool IsSelected {  get { return _Selected; } }
        private bool _Selected = false;
        public Brush BackFill { get; set; }
        public Brush StatusFill {  get { return _Model.IsAlive ? new SolidColorBrush(Colors.Transparent) : new SolidColorBrush(Colors.Black); } }
        public Brush SelectedStroke { get { return _Selected ? new SolidColorBrush(Colors.Lime) : new SolidColorBrush(Colors.Transparent); } }
        public bool IsAlive {  get { return _Model.IsAlive; } set { _Model.IsAlive = value; PropertyChanged(this, new PropertyChangedEventArgs("StatusFill")); } }
        public bool IsActive { get { return _Model.IsActive; } set { _Model.IsActive = value; PropertyChanged(this, new PropertyChangedEventArgs("IsActive")); } }
        public int Order { get { return _Model.Order; } set { _Model.Order = value; } }
        public string Name { get { return _Model.Name; } set { _Model.Name = value; PropertyChanged(this, new PropertyChangedEventArgs("Name")); } }
        public int CurrentInit { get { return _Model.CurrentInit; } set { _Model.CurrentInit = value; PropertyChanged(this, new PropertyChangedEventArgs("CurrentInit")); } }
        public int CurrentHP { get { return _Model.CurrentHP; } set { _Model.CurrentHP = value; PropertyChanged(this, new PropertyChangedEventArgs("CurrentHP")); } }
        public int TemporaryHP { get { return _Model.TemporaryHP; } set { _Model.TemporaryHP = value; PropertyChanged(this, new PropertyChangedEventArgs("TemporaryHP")); } }
        public int CurrentAC { get { return _Model.CurrentAC; } set { _Model.CurrentAC = value; PropertyChanged(this, new PropertyChangedEventArgs("CurrentAC")); } }
        public List<Enumerations.Conditions> GetConditions()
        {
            return _Model.Conditions;
        }
        public void SetConditions(List<Enumerations.Conditions> conditions)
        {
            _Model.Conditions = conditions;
            PropertyChanged(this, new PropertyChangedEventArgs("Conditions"));
        }
        public string Conditions
        {
            get
            {
                string conditions = string.Empty;
                foreach (var condition in _Model.Conditions)
                {
                    conditions += condition;
                    if (_Model.Conditions.Last() != condition)
                        conditions += ", ";
                }
                return conditions;
            }
        }
    }
}
