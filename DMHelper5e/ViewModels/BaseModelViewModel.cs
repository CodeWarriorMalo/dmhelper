﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
using System.ComponentModel;
namespace DMHelper5e.ViewModels
{
    public class BaseModelViewModel : INotifyPropertyChanged
    {
        private BaseModel _Model;
        public BaseModel Model { get { return _Model; } set { _Model = value; } }

        public event PropertyChangedEventHandler PropertyChanged;

        public BaseModelViewModel(BaseModel model)
        {
             Model = model;
        }
        public string Name {  get { return _Model.Name; } set { _Model.Name = value; PropertyChanged(this, new PropertyChangedEventArgs("Name")); } }
        public string Description { get { return _Model.Description; } set { _Model.Description = value; PropertyChanged(this, new PropertyChangedEventArgs("Description")); } }
        public int Id { get { return _Model.Id; } set { _Model.Id = value; PropertyChanged(this, new PropertyChangedEventArgs("Id")); } }

    }
}
