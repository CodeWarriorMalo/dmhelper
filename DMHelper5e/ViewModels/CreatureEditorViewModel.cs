﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
using DnD5eData.Tools;
using System.ComponentModel;
using DnD5eData.DataAccess;
using DnD5eData.DataAccess.Requests;
using DnD5eData.DataAccess.Responses;
namespace DMHelper5e.ViewModels
{
    public class CreatureEditorViewModel : INotifyPropertyChanged
    {
        private CreatureModel _Model;

        public event PropertyChangedEventHandler PropertyChanged;

        #region Model
        public CreatureModel Model
        {
            get
            {
                return _Model;
            }
            set
            {
                _Model = value;
            }
        }
        #endregion
        #region c_tor()
        public CreatureEditorViewModel()
        {
            Model = CreatureModel.CreateNew();

        }
        public CreatureEditorViewModel(CreatureModel model)
        {
            Model = model;
        }
        #endregion
        #region Senses and PP
        public string Senses
        {
            get
            {
                return _Model.Senses;
            }
            set
            {
                _Model.Senses = value;
            }
        }
        public string PassivePerception
        {
            get
            {
                return string.Format("{0}", _Model.Skills.First(p => p.Skill == Enumerations.Skills.Perception).Bonus + 10);
            }
        }
        #endregion
        #region Name, Alignment, CreatureType, and Size
        public string Name { get { return _Model.Name; } set { _Model.Name = value; } }
        public Enumerations.Alignments Alignment { get { return _Model.Alignment; } set { _Model.Alignment = value; } }
        public Enumerations.Types CreatureType { get { return _Model.CreatureType; } set { _Model.CreatureType = value; } }
        public Enumerations.Sizes Size { get { return _Model.Size; } set { _Model.Size = value; } }
        #endregion
        #region ArmorClass
        public string ArmorClass
        {
            get { return _Model.ArmorClass.ToString(); }
            set
            {
                int ac = _Model.ArmorClass;
                if (int.TryParse(value, out ac)) _Model.ArmorClass = ac;
            }
        }
        #endregion
        #region HitPoints
        public string HitPoints
        {
            get { return _Model.HitPoints.ToString(); }
            set
            {
                int hp = _Model.HitPoints;
                if (int.TryParse(value, out hp)) _Model.HitPoints = hp;
            }
        }
        #endregion
        #region HitPointsCalcDice
        public string HitPointsCalcDice
        {
            get { return _Model.HitPointCalc.NumberOfDice.ToString(); }
            set
            {
                int dice = _Model.HitPointCalc.NumberOfDice;
                if (int.TryParse(value, out dice)) _Model.HitPointCalc.NumberOfDice = dice;
            }
        }
        #endregion
        #region HitPointsCalcBonus
        public string HitPointsCalcBonus
        {
            get { return _Model.HitPointCalc.Bonus.ToString(); }
            set
            {
                int bonus = _Model.HitPointCalc.Bonus;
                if (int.TryParse(value, out bonus)) _Model.HitPointCalc.Bonus = bonus;
            }
        }
        #endregion
        #region RecalcSkills
        public void RecalcSkills()
        {
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.StrSaves).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.StrSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Strength).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.StrSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Strength).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("StrSaveSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Athletics).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Athletics).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Strength).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Athletics).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Strength).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("AthleticsSkill"));

            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.ConSaves).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.ConSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Constitution).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.ConSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Constitution).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("ConSaveSkill"));

            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.DexSaves).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.DexSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.DexSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("DexSaveSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Acrobatics).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Acrobatics).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Acrobatics).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("AcrobaticsSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.SleightOfHand).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.SleightOfHand).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.SleightOfHand).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("SleightOfHandSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Stealth).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Stealth).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Stealth).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("StealthSkill"));

            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.IntSaves).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.IntSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.IntSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("IntSaveSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Arcana).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Arcana).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Arcana).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("ArcanaSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.History).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.History).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.History).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("HistorySkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Investigation).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Investigation).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Investigation).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("InvestigationSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Nature).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Nature).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Nature).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("NatureSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Religion).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Religion).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Religion).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("ReligionSkill"));

            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.WisSaves).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.WisSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.WisSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("WisSaveSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.AnimalHandling).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.AnimalHandling).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.AnimalHandling).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("AnimalHandlingSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Insight).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Insight).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Insight).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("InsightSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Medicine).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Medicine).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Medicine).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("MedicineSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Perception).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Perception).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Perception).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("PerceptionSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Survival).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Survival).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Survival).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("SurvivalSkill"));

            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.ChaSaves).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.ChaSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.ChaSaves).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("ChaSaveSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Deception).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Deception).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Deception).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("DeceptionSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Intimidation).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Intimidation).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Intimidation).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("IntimidationSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Performance).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Performance).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Performance).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("PerformanceSkill"));
            if (_Model.Skills.First(p => p.Skill == Enumerations.Skills.Persuasion).IsTrained)
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Persuasion).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus + _Model.ProficiencyBonus;
            else
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Persuasion).Bonus =
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus;
            PropertyChanged(this, new PropertyChangedEventArgs("PersuasionSkill"));

            PropertyChanged(this, new PropertyChangedEventArgs("PassivePerception"));
        }
        #endregion
        #region Strength
        public string Strength
        {
            get { return _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Strength).Value.ToString(); }
            set
            {
                int val = _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Strength).Value;
                if (int.TryParse(value, out val))
                {
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Strength).Value = val;
                    int half = val / 2;
                    int bonus = half - 5;
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Strength).Bonus = bonus;
                    PropertyChanged(this, new PropertyChangedEventArgs("StrBonus"));
                    RecalcSkills();
                }
            }
        }
        #endregion
        #region Constitution
        public string Constitution
        {
            get { return _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Constitution).Value.ToString(); }
            set
            {
                int val = _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Constitution).Value;
                if (int.TryParse(value, out val))
                {
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Constitution).Value = val;
                    int half = val / 2;
                    int bonus = half - 5;
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Constitution).Bonus = bonus;
                    PropertyChanged(this, new PropertyChangedEventArgs("ConBonus"));
                    RecalcSkills();
                }
            }
        }
        #endregion
        #region Dexterity
        public string Dexterity
        {
            get { return _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Value.ToString(); }
            set
            {
                int val = _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Value;
                if (int.TryParse(value, out val))
                {
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Value = val;
                    int half = val / 2;
                    int bonus = half - 5;
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus = bonus;
                    PropertyChanged(this, new PropertyChangedEventArgs("DexBonus"));
                    RecalcSkills();
                }
            }
        }
        #endregion
        #region Intelligence
        public string Intelligence
        {
            get { return _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Value.ToString(); }
            set
            {
                int val = _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Value;
                if (int.TryParse(value, out val))
                {
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Value = val;
                    int half = val / 2;
                    int bonus = half - 5;
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus = bonus;
                    PropertyChanged(this, new PropertyChangedEventArgs("IntBonus"));
                    RecalcSkills();
                }
            }
        }
        #endregion
        #region Wisdom
        public string Wisdom
        {
            get { return _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Value.ToString(); }
            set
            {
                int val = _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Value;
                if (int.TryParse(value, out val))
                {
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Value = val;
                    int half = val / 2;
                    int bonus = half - 5;
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus = bonus;
                    PropertyChanged(this, new PropertyChangedEventArgs("WisBonus"));
                    RecalcSkills();
                }
            }
        }
        #endregion
        #region Charisma
        public string Charisma
        {
            get { return _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Value.ToString(); }
            set
            {
                int val = _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Value;
                if (int.TryParse(value, out val))
                {
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Value = val;
                    int half = val / 2;
                    int bonus = half - 5;
                    _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus = bonus;
                    PropertyChanged(this, new PropertyChangedEventArgs("ChaBonus"));
                    RecalcSkills();
                }
            }
        }
        #endregion
        #region Bonuses
        public string StrBonus { get { return string.Format("({0}{1})", _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Strength).Bonus > 0 ? "+" : string.Empty, _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Strength).Bonus); } }
        public string DexBonus { get { return string.Format("({0}{1})", _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus > 0 ? "+" : string.Empty, _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Dexterity).Bonus); } }
        public string ConBonus { get { return string.Format("({0}{1})", _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Constitution).Bonus > 0 ? "+" : string.Empty, _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Constitution).Bonus); } }
        public string IntBonus { get { return string.Format("({0}{1})", _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus > 0 ? "+" : string.Empty, _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Intelligence).Bonus); } }
        public string WisBonus { get { return string.Format("({0}{1})", _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus > 0 ? "+" : string.Empty, _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Wisdom).Bonus); } }
        public string ChaBonus { get { return string.Format("({0}{1})", _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus > 0 ? "+" : string.Empty, _Model.Attributes.First(p => p.Attribute == Enumerations.Attributes.Charisma).Bonus); } }
        public Enumerations.Dice HitPointsCalcDie { get { return _Model.HitPointCalc.Die; } set { _Model.HitPointCalc.Die = value; } }
        #endregion
        public string ArmorString
        {
            get
            {
                string armorstring = string.Empty;
                foreach(var armor in _Model.Armor)
                {
                    armorstring += string.Format("{0} ({1})",armor.Name, armor.Description);
                    if (_Model.Armor.Last() != armor) armorstring += "; ";
                }
                return armorstring;
            }
        }
        #region ChallengeRating
        public string ChallengeRating
        {
            get
            {
                if (_Model.ChallengeRating == 1 / 8) return "1/8";
                if (_Model.ChallengeRating == 1 / 4) return "1/4";
                if (_Model.ChallengeRating == 1 / 2) return "1/2";
                return ((int)_Model.ChallengeRating).ToString();
            }
            set
            {
                float cr = _Model.ChallengeRating;
                if (float.TryParse(value, out cr))
                {
                    _Model.ChallengeRating = cr;
                    if (_Model.ChallengeRating < 4) _Model.ProficiencyBonus = 2;
                    else if (_Model.ChallengeRating < 9) _Model.ProficiencyBonus = 3;
                    else if (_Model.ChallengeRating < 13) _Model.ProficiencyBonus = 4;
                    else if (_Model.ChallengeRating < 17) _Model.ProficiencyBonus = 5;
                    else if (_Model.ChallengeRating < 21) _Model.ProficiencyBonus = 6;
                    else if (_Model.ChallengeRating < 25) _Model.ProficiencyBonus = 7;
                    else if (_Model.ChallengeRating < 29) _Model.ProficiencyBonus = 8;
                    else _Model.ProficiencyBonus = 9;
                    PropertyChanged(this, new PropertyChangedEventArgs("ProficiencyBonus"));
                    switch (value)
                    {
                        case "0": Reward = "10"; break;
                        case "1/8": Reward = "25"; break;
                        case "1/4": Reward = "50"; break;
                        case "1/2": Reward = "100"; break;
                        case "1": Reward = "200"; break;
                        case "2": Reward = "450"; break;
                        case "3": Reward = "700"; break;
                        case "4": Reward = "1100"; break;
                        case "5": Reward = "1800"; break;
                        case "6": Reward = "2300"; break;
                        case "7": Reward = "2900"; break;
                        case "8": Reward = "3900"; break;
                        case "9": Reward = "5000"; break;
                        case "10": Reward = "5900"; break;
                        case "11": Reward = "7200"; break;
                        case "12": Reward = "8400"; break;
                        case "13": Reward = "10000"; break;
                        case "14": Reward = "11500"; break;
                        case "15": Reward = "13000"; break;
                        case "16": Reward = "15000"; break;
                        case "17": Reward = "18000"; break;
                        case "18": Reward = "20000"; break;
                        case "19": Reward = "22000"; break;
                        case "20": Reward = "25000"; break;
                        case "21": Reward = "33000"; break;
                        case "22": Reward = "41000"; break;
                        case "23": Reward = "50000"; break;
                        case "24": Reward = "62000"; break;
                        case "25": Reward = "75000"; break;
                        case "26": Reward = "90000"; break;
                        case "27": Reward = "105000"; break;
                        case "28": Reward = "120000"; break;
                        case "29": Reward = "135000"; break;
                        case "30": Reward = "155000"; break;
                    }
                    PropertyChanged(this, new PropertyChangedEventArgs("Reward"));
                    RecalcSkills();
                }
            }
        }
        #endregion
        #region ProficiencyBonus
        public string ProficiencyBonus { get { return string.Format("+{0}", _Model.ProficiencyBonus); } }
        #endregion
        #region Reward
        public string Reward
        {
            get { return _Model.Value.ToString(); }
            set
            {
                int val = _Model.Value;
                if (int.TryParse(value, out val))
                {
                    _Model.Value = val;
                }
            }
        }
        #endregion
        #region ChallengeRatings
        public List<string> ChallengeRatings
        {
            get
            {
                List<string> ratings = new List<string>();
                ratings.Add("0");
                ratings.Add("1/8");
                ratings.Add("1/4");
                ratings.Add("1/2");
                for (int r = 1; r < 31; r++)
                    ratings.Add(r.ToString());
                return ratings;
            }
        }
        #endregion
        #region Alignments Enum
        public List<Enumerations.Alignments> Alignments
        {
            get
            {
                List<Enumerations.Alignments> alignments = new List<Enumerations.Alignments>();
                alignments.Add(Enumerations.Alignments.LawfulGood);
                alignments.Add(Enumerations.Alignments.LawfulNeutral);
                alignments.Add(Enumerations.Alignments.LawfulEvil);
                alignments.Add(Enumerations.Alignments.NeutralGood);
                alignments.Add(Enumerations.Alignments.TrueNeutral);
                alignments.Add(Enumerations.Alignments.NeutralEvil);
                alignments.Add(Enumerations.Alignments.ChaoticGood);
                alignments.Add(Enumerations.Alignments.ChaoticNeutral);
                alignments.Add(Enumerations.Alignments.ChaoticEvil);
                return alignments;
            }
        }
        #endregion
        #region Sizes Enum
        public List<Enumerations.Sizes> Sizes
        {
            get
            {
                List<Enumerations.Sizes> sizes = new List<Enumerations.Sizes>();
                sizes.Add(Enumerations.Sizes.Diminutive);
                sizes.Add(Enumerations.Sizes.Fine);
                sizes.Add(Enumerations.Sizes.Tiny);
                sizes.Add(Enumerations.Sizes.Small);
                sizes.Add(Enumerations.Sizes.Medium);
                sizes.Add(Enumerations.Sizes.Large);
                sizes.Add(Enumerations.Sizes.Huge);
                sizes.Add(Enumerations.Sizes.Gargantuan);
                sizes.Add(Enumerations.Sizes.Colossal);
                return sizes;
            }
        }
        #endregion
        #region CreatureTypes Enum
        public List<Enumerations.Types> CreatureTypes
        {
            get
            {
                List<Enumerations.Types> Types = new List<Enumerations.Types>();
                Types.Add(Enumerations.Types.Aberration);
                Types.Add(Enumerations.Types.Beast);
                Types.Add(Enumerations.Types.Celestial);
                Types.Add(Enumerations.Types.Construct);
                Types.Add(Enumerations.Types.Dragon);
                Types.Add(Enumerations.Types.Elemental);
                Types.Add(Enumerations.Types.Fey);
                Types.Add(Enumerations.Types.Fiend);
                Types.Add(Enumerations.Types.Giant);
                Types.Add(Enumerations.Types.Humanoid);
                Types.Add(Enumerations.Types.Monstrosity);
                Types.Add(Enumerations.Types.Ooze);
                Types.Add(Enumerations.Types.Plant);
                Types.Add(Enumerations.Types.Undead);
                return Types;
            }
        }
        #endregion
        #region Dice Enum
        public List<Enumerations.Dice> Dice
        {
            get
            {
                List<Enumerations.Dice> Dice = new List<Enumerations.Dice>();
                Dice.Add(Enumerations.Dice.d4);
                Dice.Add(Enumerations.Dice.d6);
                Dice.Add(Enumerations.Dice.d8);
                Dice.Add(Enumerations.Dice.d10);
                Dice.Add(Enumerations.Dice.d12);
                Dice.Add(Enumerations.Dice.d20);
                Dice.Add(Enumerations.Dice.d100);
                return Dice;
            }
        }
        #endregion
        #region STR Skills
        public bool StrSaveTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.StrSaves).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.StrSaves).IsTrained = value;
                RecalcSkills();
            }
        }
        public string StrSaveSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.StrSaves).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.StrSaves).Bonus);
            }
        }
        public bool AthleticsTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Athletics).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Athletics).IsTrained = value;
                RecalcSkills();
            }
        }
        public string AthleticsSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Athletics).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Athletics).Bonus);
            }
        }
        #endregion
        #region DEX Skills
        public bool DexSaveTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.DexSaves).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.DexSaves).IsTrained = value;
                RecalcSkills();
            }
        }
        public string DexSaveSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.DexSaves).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.DexSaves).Bonus);
            }
        }
        public bool AcrobaticsTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Acrobatics).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Acrobatics).IsTrained = value;
                RecalcSkills();
            }
        }
        public string AcrobaticsSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Acrobatics).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Acrobatics).Bonus);
            }
        }
        public bool SleightOfHandTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.SleightOfHand).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.SleightOfHand).IsTrained = value;
                RecalcSkills();
            }
        }
        public string SleightOfHandSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.SleightOfHand).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.SleightOfHand).Bonus);
            }
        }
        public bool StealthTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Stealth).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Stealth).IsTrained = value;
                RecalcSkills();
            }
        }
        public string StealthSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Stealth).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Stealth).Bonus);
            }
        }
        #endregion
        #region CON Skills
        public bool ConSaveTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.ConSaves).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.ConSaves).IsTrained = value;
                RecalcSkills();
            }
        }
        public string ConSaveSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.ConSaves).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.ConSaves).Bonus);
            }
        }
        #endregion
        #region INT Skills
        public bool IntSaveTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.IntSaves).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.IntSaves).IsTrained = value;
                RecalcSkills();
            }
        }
        public string IntSaveSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.IntSaves).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.IntSaves).Bonus);
            }
        }
        public bool ArcanaTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Arcana).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Arcana).IsTrained = value;
                RecalcSkills();
            }
        }
        public string ArcanaSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Arcana).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Arcana).Bonus);
            }
        }
        public bool HistoryTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.History).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.History).IsTrained = value;
                RecalcSkills();
            }
        }
        public string HistorySkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.History).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.History).Bonus);
            }
        }
        public bool InvestigationTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Investigation).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Investigation).IsTrained = value;
                RecalcSkills();
            }
        }
        public string InvestigationSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Investigation).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Investigation).Bonus);
            }
        }
        public bool NatureTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Nature).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Nature).IsTrained = value;
                RecalcSkills();
            }
        }
        public string NatureSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Nature).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Nature).Bonus);
            }
        }
        public bool ReligionTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Religion).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Religion).IsTrained = value;
                RecalcSkills();
            }
        }
        public string ReligionSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Religion).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Religion).Bonus);
            }
        }
        #endregion
        #region WIS Skills
        public bool WisSaveTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.WisSaves).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.WisSaves).IsTrained = value;
                RecalcSkills();
            }
        }
        public string WisSaveSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.WisSaves).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.WisSaves).Bonus);
            }
        }
        public bool AnimalHandlingTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.AnimalHandling).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.AnimalHandling).IsTrained = value;
                RecalcSkills();
            }
        }
        public string AnimalHandlingSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.AnimalHandling).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.AnimalHandling).Bonus);
            }
        }
        public bool InsightTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Insight).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Insight).IsTrained = value;
                RecalcSkills();
            }
        }
        public string InsightSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Insight).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Insight).Bonus);
            }
        }
        public bool MedicineTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Medicine).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Medicine).IsTrained = value;
                RecalcSkills();
            }
        }
        public string MedicineSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Medicine).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Medicine).Bonus);
            }
        }
        public bool PerceptionTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Perception).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Perception).IsTrained = value;
                RecalcSkills();
            }
        }
        public string PerceptionSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Perception).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Perception).Bonus);
            }
        }
        public bool SurvivalTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Survival).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Survival).IsTrained = value;
                RecalcSkills();
            }
        }
        public string SurvivalSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Survival).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Survival).Bonus);
            }
        }
        #endregion
        #region CHA Skills
        public bool ChaSaveTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.ChaSaves).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.ChaSaves).IsTrained = value;
                RecalcSkills();
            }
        }
        public string ChaSaveSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.ChaSaves).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.ChaSaves).Bonus);
            }
        }
        public bool DeceptionTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Deception).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Deception).IsTrained = value;
                RecalcSkills();
            }
        }
        public string DeceptionSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Deception).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Deception).Bonus);
            }
        }
        public bool IntimidationTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Intimidation).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Intimidation).IsTrained = value;
                RecalcSkills();
            }
        }
        public string IntimidationSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Intimidation).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Intimidation).Bonus);
            }
        }
        public bool PerformanceTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Performance).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Performance).IsTrained = value;
                RecalcSkills();
            }
        }
        public string PerformanceSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Performance).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Performance).Bonus);
            }
        }
        public bool PersuasionTraining
        {
            get { return _Model.Skills.First(p => p.Skill == Enumerations.Skills.Persuasion).IsTrained; }
            set
            {
                _Model.Skills.First(p => p.Skill == Enumerations.Skills.Persuasion).IsTrained = value;
                RecalcSkills();
            }
        }
        public string PersuasionSkill
        {
            get
            {
                return string.Format("({0}{1})",
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Persuasion).Bonus > 0 ? "+" : string.Empty,
                    _Model.Skills.First(p => p.Skill == Enumerations.Skills.Persuasion).Bonus);
            }
        }
        #endregion
        #region Damage and Conditions
        public void SetDamageResistances(List<Enumerations.DamageTypes> list)
        {
            _Model.DamageResistances = list;
            PropertyChanged(this, new PropertyChangedEventArgs("DamageResistances"));
        }
        public List<Enumerations.DamageTypes> GetDamageResistances()
        {
            return _Model.DamageResistances;
        }
        public string DamageResistances
        {
            get
            {
                string val = string.Empty;
                foreach (var item in _Model.DamageResistances)
                {
                    if (_Model.DamageResistances.Last() == item)
                        val += item;
                    else
                        val += item + ",";
                }
                return val;
            }
        }
        public void SetConditionResistances(List<Enumerations.Conditions> list)
        {
            _Model.ConditionResistances = list;
            PropertyChanged(this, new PropertyChangedEventArgs("ConditionResistances"));
        }
        public List<Enumerations.Conditions> GetConditionResistances()
        {
            return _Model.ConditionResistances;
        }
        public string ConditionResistances
        {
            get
            {
                string val = string.Empty;
                foreach (var item in _Model.ConditionResistances)
                {
                    if (_Model.ConditionResistances.Last() == item)
                        val += item;
                    else
                        val += item + ",";
                }
                return val;
            }
        }
        public void SetDamageImmunities(List<Enumerations.DamageTypes> list)
        {
            _Model.DamageImmunities = list;
            PropertyChanged(this, new PropertyChangedEventArgs("DamageImmunities"));
        }
        public List<Enumerations.DamageTypes> GetDamageImmunities()
        {
            return _Model.DamageImmunities;
        }
        public string DamageImmunities
        {
            get
            {
                string val = string.Empty;
                foreach (var item in _Model.DamageImmunities)
                {
                    if (_Model.DamageImmunities.Last() == item)
                        val += item;
                    else
                        val += item + ",";
                }
                return val;
            }
        }
        public void SetConditionImmunities(List<Enumerations.Conditions> list)
        {
            _Model.ConditionImmunities = list;
            PropertyChanged(this, new PropertyChangedEventArgs("ConditionImmunities"));
        }
        public List<Enumerations.Conditions> GetConditionImmunities()
        {
            return _Model.ConditionImmunities;
        }
        public string ConditionImmunities
        {
            get
            {
                string val = string.Empty;
                foreach (var item in _Model.ConditionImmunities)
                {
                    if (_Model.ConditionImmunities.Last() == item)
                        val += item;
                    else
                        val += item + ",";
                }
                return val;
            }
        }
        public void SetDamageVunerabilities(List<Enumerations.DamageTypes> list)
        {
            _Model.DamageVunerabilities = list;
            PropertyChanged(this, new PropertyChangedEventArgs("DamageVunerabilities"));
        }
        public List<Enumerations.DamageTypes> GetDamageVunerabilities()
        {
            return _Model.DamageVunerabilities;
        }
        public string DamageVunerabilities
        {
            get
            {
                string val = string.Empty;
                foreach (var item in _Model.DamageVunerabilities)
                {
                    if (_Model.DamageVunerabilities.Last() == item)
                        val += item;
                    else
                        val += item + ",";
                }
                return val;
            }
        }
        public void SetConditionVunerabilities(List<Enumerations.Conditions> list)
        {
            _Model.ConditionVunerabilities = list;
            PropertyChanged(this, new PropertyChangedEventArgs("ConditionVunerabilities"));
        }
        public List<Enumerations.Conditions> GetConditionVunerabilities()
        {
            return _Model.ConditionVunerabilities;
        }
        public string ConditionVunerabilities
        {
            get
            {
                string val = string.Empty;
                foreach (var item in _Model.ConditionVunerabilities)
                {
                    if (_Model.ConditionVunerabilities.Last() == item)
                        val += item;
                    else
                        val += item + ",";
                }
                return val;
            }
        }
        #endregion
        #region Languages
        public List<LanguageModel> GetLanguages()
        {
            return _Model.Languages;
        }
        public void SetLanguages(List<LanguageModel> languages)
        {
            _Model.Languages = languages;
            PropertyChanged(this, new PropertyChangedEventArgs("Languages"));
        }
        public string Languages
        {
            get
            {
                string val = string.Empty;
                foreach(var lang in _Model.Languages)
                {
                    if (_Model.Languages.Last() == lang)
                        val += lang.Name;
                    else
                        val += string.Format("{0},", lang.Name);
                }
                return val;
            }
        }
        #endregion
    }
}
