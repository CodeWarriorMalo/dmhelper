﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using DnD5eData.Models;
using DnD5eData.Tools;
namespace DMHelper5e.ViewModels
{
    public class WeaponModelViewModel : INotifyPropertyChanged
    {
        private WeaponModel _Model;
        public WeaponModel Model
        {
            get { return _Model; }
            set { _Model = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public WeaponModelViewModel(WeaponModel model)
        {
            Model = model;
        }
        public string Name { get { return _Model.Name; } set { _Model.Name = value; PropertyChanged(this, new PropertyChangedEventArgs("Name")); } }
        public string Description { get { return _Model.Description; } set { _Model.Description = value; PropertyChanged(this, new PropertyChangedEventArgs("Description")); } }
        public int Id { get { return _Model.Id; } set { _Model.Id = value; PropertyChanged(this, new PropertyChangedEventArgs("Id")); } }
        public string HitPlusMinus { get { return _Model.AttackBonus < 0 ? "-" : "+"; } }
        public int AttackBonus { get { return _Model.AttackBonus; } set { _Model.AttackBonus = value; PropertyChanged(this, new PropertyChangedEventArgs("AttackBonus")); } }
        public string DmgPlusMinus { get { return _Model.DamageBonus < 0 ? "-" : "+"; } }
        public int DamageBonus { get { return _Model.DamageBonus; } set { _Model.DamageBonus = value; PropertyChanged(this, new PropertyChangedEventArgs("DamageBonus")); } }
        public Enumerations.DamageTypes DamageType { get { return _Model.DamageType; } set { _Model.DamageType = value; PropertyChanged(this, new PropertyChangedEventArgs("DamageType")); } }
        public Enumerations.Dice Die { get { return _Model.Die; } set { _Model.Die = value; PropertyChanged(this, new PropertyChangedEventArgs("Die")); } }
        public bool IsTwoHanded { get { return _Model.IsTwoHanded; } set { _Model.IsTwoHanded = value; PropertyChanged(this, new PropertyChangedEventArgs("IsTwoHanded")); } }
        public bool IsVersitile { get { return _Model.IsVersitile; } set { _Model.IsVersitile = value; PropertyChanged(this, new PropertyChangedEventArgs("IsVersitile")); } }
        public int NumberOfDice { get { return _Model.NumberOfDice; } set { _Model.NumberOfDice = value; PropertyChanged(this, new PropertyChangedEventArgs("NumberOfDice")); } }
        public Enumerations.WeaponClasses WeaponClass { get { return _Model.WeaponClass; } set { _Model.WeaponClass = value; PropertyChanged(this, new PropertyChangedEventArgs("WeaponClass")); } }
    }
}
