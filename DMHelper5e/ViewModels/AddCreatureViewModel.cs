﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.DataAccess;
using DnD5eData.DataAccess.Requests;
using DnD5eData.DataAccess.Responses;
using DnD5eData.Models;
using DnD5eData.Tools;

namespace DMHelper5e.ViewModels
{
    public class AddCreatureViewModel
    {
        private List<CreatureModel> _Monsters;
        private List<CreatureModel> _Players;
        private List<CreatureModel> _NPCs;
        public int NumberOfCreatures = 0;
        public bool DialogResult;
        public CreatureModel Result
        {
            get
            {
                if (Side == Enumerations.CreatureSides.Enemy)
                    return _Monsters.First(m => m.Name == Item);
                else if (Side == Enumerations.CreatureSides.Ally)
                    return _Players.First(p => p.Name == Item);
                else if (Side == Enumerations.CreatureSides.Neutral)
                    return _NPCs.First(n => n.Name == Item);

                return new CreatureModel();
            }
        }
        public AddCreatureViewModel()
        {
            RetrieveMonsterRequest monsterReq = RetrieveMonsterRequest.CreateNew();
            RetrieveMonsterResponse monsterResp = DatabaseAccess.Instance.RetrieveMonster(monsterReq);
            if (monsterResp.Success) _Monsters = monsterResp.Monsters;
            else _Monsters = new List<CreatureModel>();

            RetrievePlayerRequest playerReq = RetrievePlayerRequest.CreateNew();
            RetrievePlayerResponse playerResp = DatabaseAccess.Instance.RetrievePlayer(playerReq);
            if (playerResp.Success) _Players = playerResp.Players;
            else _Players = new List<CreatureModel>();

            RetrieveNPCRequest npcReq = RetrieveNPCRequest.CreateNew();
            RetrieveNPCResponse npcResp = DatabaseAccess.Instance.RetrieveNPC(npcReq);
            if (npcResp.Success) _NPCs = npcResp.NPCs;
            else _NPCs = new List<CreatureModel>();
        }
        public List<Enumerations.CreatureSides> Sides
        {
            get
            {
                List<Enumerations.CreatureSides> sides = new List<Enumerations.CreatureSides>();
                sides.Add(Enumerations.CreatureSides.Ally);
                sides.Add(Enumerations.CreatureSides.Enemy);
                sides.Add(Enumerations.CreatureSides.Neutral);
                return sides;
            }
        }

        public Enumerations.CreatureSides Side { get; set; }
        public string Number
        {
            get { return NumberOfCreatures.ToString(); }
            set
            {
                int noc = NumberOfCreatures;
                if (int.TryParse(value, out noc)) NumberOfCreatures = noc;
            }
        }
        public List<string> Items
        {
            get
            {
                List<string> items = new List<string>();
                if(Side == Enumerations.CreatureSides.Enemy)
                {
                    foreach (var monster in _Monsters)
                        items.Add(monster.Name);
                }
                else if (Side == Enumerations.CreatureSides.Ally)
                {
                    foreach (var player in _Players)
                        items.Add(player.Name);
                }
                else if (Side == Enumerations.CreatureSides.Neutral)
                {
                    foreach (var npc in _NPCs)
                        items.Add(npc.Name);
                }
                return items;
            }
        }

        public string Item { get; set; }

    }
}
