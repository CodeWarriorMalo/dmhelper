﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using DnD5eData.Models;
using DnD5eData.Tools;
namespace DMHelper5e.ViewModels
{
    public class AttackModelViewModel : INotifyPropertyChanged
    {
        private AttackModel _Model;
        public AttackModel Model
        {
            get { return _Model; }
            set { _Model = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public AttackModelViewModel(AttackModel model)
        {
            Model = model;
        }
        public string Name { get { return _Model.Name; } set { _Model.Name = value; PropertyChanged(this, new PropertyChangedEventArgs("Name")); } }
        public string Description { get { return _Model.Description; } set { _Model.Description = value; PropertyChanged(this, new PropertyChangedEventArgs("Description")); } }
        public int Id { get { return _Model.Id; } set { _Model.Id = value; PropertyChanged(this, new PropertyChangedEventArgs("Id")); } }
        public int AttackBonus { get { return _Model.AttackBonus; } set { _Model.AttackBonus = value; PropertyChanged(this, new PropertyChangedEventArgs("AttackBonus")); } }
        public int DamageBonus { get { return _Model.DamageBonus; } set { _Model.DamageBonus = value; PropertyChanged(this, new PropertyChangedEventArgs("DamageBonus")); } }
        public string Weapon { get { return Tools.GetWeaponName(_Model.WeaponId); } set { _Model.WeaponId = Tools.GetWeaponId(value); PropertyChanged(this, new PropertyChangedEventArgs("Weapon")); } }
    }
}
