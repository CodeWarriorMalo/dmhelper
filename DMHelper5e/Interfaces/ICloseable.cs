﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DMHelper5e.Interfaces
{
    public delegate void ClosingEventHandler(bool saving);
    public interface ICloseable
    {
        event ClosingEventHandler OnClose;
    }
}
