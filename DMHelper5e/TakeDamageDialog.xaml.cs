﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for TakeDamageDialog.xaml
    /// </summary>
    public partial class TakeDamageDialog : Window
    {
        public TakeDamageDialog()
        {
            InitializeComponent();
        }
        public bool Affirmative;
        private int _Damage;
        public int Damage
        {
            get
            {
                int.TryParse(txtTakeDamage.Text, out _Damage);
                return _Damage;
            }
            set
            {
                _Damage = value;
                txtTakeDamage.Text = _Damage.ToString();
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Affirmative = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Affirmative = false;
            Close();
        }
    }
}
