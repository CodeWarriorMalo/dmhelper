﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DnD5eData.Tools;
using DMHelper5e.Views;
namespace DMHelper5e
{
    /// <summary>
    /// Interaction logic for DamageTypeEditorWindow.xaml
    /// </summary>
    public partial class DamageTypeEditorWindow : Window
    {
        private DamageTypeEditorView _View;
        public List<Enumerations.DamageTypes> List {  get { return _View.List; } }
        public DamageTypeEditorWindow(List<Enumerations.DamageTypes> list, Enumerations.Editors editor)
        {
            InitializeComponent();
            _View = new DamageTypeEditorView(list);
            _View.OnClose += _View_OnClose;
            mainGrid.Children.Add(_View);
            this.Title = string.Format("Damage {0} Editor", editor);
        }
        public bool Saved;
        private void _View_OnClose(bool saving)
        {
            Saved = saving;
            this.Close();
        }
    }
}
