﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMHelper5e.ViewModels;
using DnD5eData.Tools;
using DnD5eData.Models;
namespace DMHelper5e.Views
{
    /// <summary>
    /// Interaction logic for EncounterControl.xaml
    /// </summary>
    public partial class EncounterControl : UserControl
    {
        public delegate void SelectedEncounterItemChangedEventHandler(string name);
        public event SelectedEncounterItemChangedEventHandler SelectedEncounterItemChanged;
        private List<EncounterListItem> _Items;
        public EncounterControl(EncounterModel encounter)
        {
            InitializeComponent();
            _Items = new List<EncounterListItem>();
            foreach(var item in encounter.EncounterItems)
            {
                EncounterListItem encounterItem = new EncounterListItem(new EncounterListItemViewModel(item));
                encounterItem.EncounterListControlItemClicked += EncounterListControlItemClicked;
                _Items.Add(encounterItem);
            }
            LoadItems();
        }

        private void EncounterListControlItemClicked(EncounterListItemViewModel viewModel)
        {
            foreach (var item in _Items)
            {
                item.ViewModel.Deselect();
            }
            viewModel.Select();
            if(SelectedEncounterItemChanged != null)
                SelectedEncounterItemChanged(viewModel.Name);
        }
        public void KillActiveCreature()
        {
            _Items.First(i => i.ViewModel.IsSelected).ViewModel.IsAlive = false;
        }
        public void ReviveActiveCreature()
        {
            _Items.First(i => i.ViewModel.IsSelected).ViewModel.IsAlive = true;
        }
        public void SelectActiveCreature()
        {
            if (_Items.Count == 0) return;
            foreach (var item in _Items)
            {
                item.ViewModel.Deselect();
            }
            _Items.First(i => i.ViewModel.IsActive).ViewModel.Select();
        }
        public string SelectedCreatureName
        {
            get
            {
                return _Items.First(i => i.ViewModel.IsSelected).ViewModel.Name;
            }
        }
        public void ActivateSelectedCreature()
        {
            foreach (var item in _Items)
            {
                item.ViewModel.IsActive = false;
            }
            _Items.First(i => i.ViewModel.IsSelected).ViewModel.IsActive = true;
        }
        public void StartNextRound()
        {
            foreach (var item in _Items.OrderBy(i => i.ViewModel.Order))
            {
                item.ViewModel.IsActive = false;
            }
            _Items.First(p => p.ViewModel.Order == 1).ViewModel.IsActive = true;
        }
        public void StartInitiative()
        {
            // launch initiative editor, then
            LoadItems();
        }
        public void SelectNextCreature()
        {
            int lastActive = _Items.First(p => p.ViewModel.IsActive).ViewModel.Order;
            foreach (var item in _Items.OrderBy(i => i.ViewModel.Order))
            {
                item.ViewModel.IsActive = false;
            }
            int newActive = lastActive + 1;
            if (newActive > _Items.Count) newActive = 1;
            _Items.First(p => p.ViewModel.Order == newActive).ViewModel.IsActive = true;
        }
        public void SelectPreviousCreature()
        {
            int lastActive = _Items.First(p => p.ViewModel.IsActive).ViewModel.Order;
            foreach (var item in _Items.OrderBy(i => i.ViewModel.Order))
            {
                item.ViewModel.IsActive = false;
            }
            int newActive = lastActive - 1;
            if (newActive < 1) newActive = _Items.Count;
            _Items.First(p => p.ViewModel.Order == newActive).ViewModel.IsActive = true;
        }
        private void LoadItems()
        {
            int toggle = -1;
            stackEncounter.Children.Clear();
            if (_Items == null || _Items.Count == 0) return;
            int order = 1;
            foreach(var item in _Items.OrderByDescending(i => i.ViewModel.CurrentInit))
            {
                item.ViewModel.Order = order;
                order++;
            }
            foreach(var item in _Items.OrderBy(i=>i.ViewModel.Order))
            {
                if (toggle == 1)
                    item.ViewModel.BackFill = new SolidColorBrush(Colors.LightGray);                stackEncounter.Children.Add(item);
                toggle *= -1;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SelectActiveCreature();
        }
    }
}
