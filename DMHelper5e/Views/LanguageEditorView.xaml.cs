﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DnD5eData.Tools;
using DnD5eData.Models;
using DMHelper5e.Interfaces;
namespace DMHelper5e.Views
{
    /// <summary>
    /// Interaction logic for LanguageEditorView.xaml
    /// </summary>
    public partial class LanguageEditorView : UserControl, ICloseable
    {
        private List<LanguageModel> _List = new List<LanguageModel>();

        public event ClosingEventHandler OnClose;

        public List<LanguageModel> List { get { { return _List; } } }
        public LanguageEditorView(List<LanguageModel> list)
        {
            InitializeComponent();
            _List = list;
            chkAbyssal.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Abyssal);
            chkCelestial.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Celestial);
            chkCommon.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Common);
            chkDeepSpeech.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.DeepSpeech);
            chkDraconic.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Draconic);
            chkDwarvish.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Dwarvish);
            chkElvish.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Elvish);
            chkGiant.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Giant);
            chkGnomish.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Gnomish);
            chkGoblin.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Goblin);
            chkHalfling.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Halfling);
            chkInfernal.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Infernal);
            chkOrc.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Orc);
            chkPrimordial.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Primordial);
            chkSylvan.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Sylvan);
            chkUndercommon.IsChecked = _List.Any(p => p.Language == Enumerations.Languages.Undercommon);
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (chkAbyssal.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Abyssal))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Abyssal,
                        Language = Enumerations.Languages.Abyssal,
                        Name = "Abyssal",
                        Script = Enumerations.Scripts.Infernal
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Abyssal))
                    _List.RemoveAll(p=>p.Language == Enumerations.Languages.Abyssal);
            }
            if (chkCelestial.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Celestial))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Celestial,
                        Language = Enumerations.Languages.Celestial,
                        Name = "Celestial",
                        Script = Enumerations.Scripts.Celestial
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Celestial))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Celestial);
            }
            if (chkCommon.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Common))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Common,
                        Language = Enumerations.Languages.Common,
                        Name = "Common",
                        Script = Enumerations.Scripts.Common
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Common))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Common);
            }
            if (chkDeepSpeech.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.DeepSpeech))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.DeepSpeech,
                        Language = Enumerations.Languages.DeepSpeech,
                        Name = "Deep Speech",
                        Script = Enumerations.Scripts.Common
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.DeepSpeech))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.DeepSpeech);
            }
            if (chkDraconic.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Draconic))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Draconic,
                        Language = Enumerations.Languages.Draconic,
                        Name = "Draconic",
                        Script = Enumerations.Scripts.Draconic
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Draconic))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Draconic);
            }
            if (chkDwarvish.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Dwarvish))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Dwarvish,
                        Language = Enumerations.Languages.Dwarvish,
                        Name = "Dwarvish",
                        Script = Enumerations.Scripts.Dwarvish
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Dwarvish))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Dwarvish);
            }
            if (chkElvish.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Elvish))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Elvish,
                        Language = Enumerations.Languages.Elvish,
                        Name = "Elvish",
                        Script = Enumerations.Scripts.Elvish
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Elvish))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Elvish);
            }
            if (chkGiant.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Giant))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Giant,
                        Language = Enumerations.Languages.Giant,
                        Name = "Giant",
                        Script = Enumerations.Scripts.Dwarvish
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Giant))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Giant);
            }
            if (chkGnomish.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Gnomish))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Gnomish,
                        Language = Enumerations.Languages.Gnomish,
                        Name = "Gnomish",
                        Script = Enumerations.Scripts.Dwarvish
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Gnomish))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Gnomish);
            }
            if (chkGoblin.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Goblin))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Goblin,
                        Language = Enumerations.Languages.Goblin,
                        Name = "Goblin",
                        Script = Enumerations.Scripts.Dwarvish
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Goblin))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Goblin);
            }
            if (chkHalfling.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Halfling))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Halfling,
                        Language = Enumerations.Languages.Halfling,
                        Name = "Halfling",
                        Script = Enumerations.Scripts.Common
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Halfling))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Halfling);
            }
            if (chkInfernal.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Infernal))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Infernal,
                        Language = Enumerations.Languages.Infernal,
                        Name = "Infernal",
                        Script = Enumerations.Scripts.Infernal
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Infernal))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Infernal);
            }
            if (chkOrc.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Orc))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Orc,
                        Language = Enumerations.Languages.Orc,
                        Name = "Orc",
                        Script = Enumerations.Scripts.Dwarvish
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Orc))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Orc);
            }
            if (chkPrimordial.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Primordial))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Primordial,
                        Language = Enumerations.Languages.Primordial,
                        Name = "Primordial",
                        Script = Enumerations.Scripts.Dwarvish
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Primordial))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Primordial);
            }
            if (chkSylvan.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Sylvan))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Sylvan,
                        Language = Enumerations.Languages.Sylvan,
                        Name = "Sylvan",
                        Script = Enumerations.Scripts.Elvish
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Sylvan))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Sylvan);
            }
            if (chkUndercommon.IsChecked.Value)
            {
                if (!_List.Any(p => p.Language == Enumerations.Languages.Undercommon))
                    _List.Add(new LanguageModel()
                    {
                        Description = "",
                        Id = (int)Enumerations.Languages.Undercommon,
                        Language = Enumerations.Languages.Undercommon,
                        Name = "Undercommon",
                        Script = Enumerations.Scripts.Elvish
                    });
            }
            else
            {
                if (_List.Any(p => p.Language == Enumerations.Languages.Undercommon))
                    _List.RemoveAll(p => p.Language == Enumerations.Languages.Undercommon);
            }

            OnClose(true);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            OnClose(false);
        }
    }
}
