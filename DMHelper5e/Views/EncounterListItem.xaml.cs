﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMHelper5e.ViewModels;
namespace DMHelper5e.Views
{
    /// <summary>
    /// Interaction logic for EncounterListItem.xaml
    /// </summary>
    public partial class EncounterListItem : UserControl
    {
        public delegate void EncounterListControlClickedEventHandler(EncounterListItemViewModel viewModel);
        public event EncounterListControlClickedEventHandler EncounterListControlItemClicked;
        private EncounterListItemViewModel _ViewModel;
        public EncounterListItemViewModel ViewModel
        {
            get { return _ViewModel; }
            set { _ViewModel = value; DataContext = _ViewModel; }
        }
        public EncounterListItem(EncounterListItemViewModel viewModel)
        {
            InitializeComponent();
            ViewModel = viewModel;
        }

        private void UserControl_MouseDown(object sender, MouseButtonEventArgs e)
        {
            EncounterListControlItemClicked(_ViewModel);
        }
    }
}
