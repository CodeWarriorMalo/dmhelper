﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMHelper5e.ViewModels;
namespace DMHelper5e.Views
{
    /// <summary>
    /// Interaction logic for CreatureEditorView.xaml
    /// </summary>
    public partial class CreatureControl : UserControl
    {
        private CreatureEditorViewModel _ViewModel;
        private Window _Owner;
        public CreatureControl(CreatureEditorViewModel viewModel, Window owner)
        {
            _ViewModel = viewModel;
            InitializeComponent();
            DataContext = _ViewModel;
            _Owner = owner;

            LoadFeatures();
            LoadActions();
            LoadReactions();
            LoadAttacks();
            LoadWeapons();
        }
        private void LoadFeatures()
        {
            stackFeatures.Children.Clear();
            foreach (var feat in _ViewModel.Model.Features)
            {
                stackFeatures.Children.Add(new BaseModelView(feat));
            }
        }
        private void LoadActions()
        {
            stackActions.Children.Clear();
            foreach (var action in _ViewModel.Model.Actions)
            {
                stackActions.Children.Add(new BaseModelView(action));
            }
        }
        private void LoadReactions()
        {
            stackReactions.Children.Clear();
            foreach (var reaction in _ViewModel.Model.Reactions)
            {
                stackReactions.Children.Add(new BaseModelView(reaction));
            }
        }
        private void LoadAttacks()
        {
            stackAttacks.Children.Clear();
            foreach (var attack in _ViewModel.Model.Attacks)
            {
                stackAttacks.Children.Add(new AttackModelView(attack));
            }
        }
        private void LoadWeapons()
        {
            stackWeapons.Children.Clear();
            foreach (var weapon in _ViewModel.Model.Weapons)
            {
                stackWeapons.Children.Add(new WeaponModelView(weapon));
            }
        }
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _ViewModel.Strength = _ViewModel.Strength;
            _ViewModel.Dexterity = _ViewModel.Dexterity;
            _ViewModel.Constitution = _ViewModel.Constitution;
            _ViewModel.Intelligence = _ViewModel.Intelligence;
            _ViewModel.Wisdom = _ViewModel.Wisdom;
            _ViewModel.Charisma = _ViewModel.Charisma;
        }
    }
}
