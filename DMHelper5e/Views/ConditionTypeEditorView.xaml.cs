﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DnD5eData.Tools;
using DnD5eData.Models;
using DMHelper5e.Interfaces;
namespace DMHelper5e.Views
{
    /// <summary>
    /// Interaction logic for ConditionTypeEditorView.xaml
    /// </summary>
    public partial class ConditionTypeEditorView : UserControl, ICloseable
    {
        private List<Enumerations.Conditions> _List = new List<Enumerations.Conditions>();

        public event ClosingEventHandler OnClose;

        public List<Enumerations.Conditions> List { get { { return _List; } } }
        public ConditionTypeEditorView(List<Enumerations.Conditions> list)
        {
            InitializeComponent();
            _List = list;
            chkBlinded.IsChecked = _List.Any(p => p == Enumerations.Conditions.Blinded);
            chkCharmed.IsChecked = _List.Any(p => p == Enumerations.Conditions.Charmed);
            chkDeafened.IsChecked = _List.Any(p => p == Enumerations.Conditions.Deafened);
            chkFatigued.IsChecked = _List.Any(p => p == Enumerations.Conditions.Fatigued);
            chkFrightened.IsChecked = _List.Any(p => p == Enumerations.Conditions.Frightened);
            chkGrappled.IsChecked = _List.Any(p => p == Enumerations.Conditions.Grappled);
            chkIncapacitated.IsChecked = _List.Any(p => p == Enumerations.Conditions.Incapacitated);
            chkInvisible.IsChecked = _List.Any(p => p == Enumerations.Conditions.Invisible);
            chkParalyzed.IsChecked = _List.Any(p => p == Enumerations.Conditions.Paralyzed);
            chkPetrified.IsChecked = _List.Any(p => p == Enumerations.Conditions.Petrified);
            chkPoisoned.IsChecked = _List.Any(p => p == Enumerations.Conditions.Poisoned);
            chkProne.IsChecked = _List.Any(p => p == Enumerations.Conditions.Prone);
            chkRestrained.IsChecked = _List.Any(p => p == Enumerations.Conditions.Restrained);
            chkStunned.IsChecked = _List.Any(p => p == Enumerations.Conditions.Stunned);
            chkUnconscious.IsChecked = _List.Any(p => p == Enumerations.Conditions.Unconscious);
            chkExhaustion.IsChecked = _List.Any(p => p == Enumerations.Conditions.Exhaustion);
        }
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (chkBlinded.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Blinded))
                    _List.Add(Enumerations.Conditions.Blinded);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Blinded))
                    _List.Remove(Enumerations.Conditions.Blinded);
            }
            if (chkCharmed.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Charmed))
                    _List.Add(Enumerations.Conditions.Charmed);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Charmed))
                    _List.Remove(Enumerations.Conditions.Charmed);
            }
            if (chkDeafened.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Deafened))
                    _List.Add(Enumerations.Conditions.Deafened);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Deafened))
                    _List.Remove(Enumerations.Conditions.Deafened);
            }
            if (chkExhaustion.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Exhaustion))
                    _List.Add(Enumerations.Conditions.Exhaustion);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Exhaustion))
                    _List.Remove(Enumerations.Conditions.Exhaustion);
            }
            if (chkFatigued.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Fatigued))
                    _List.Add(Enumerations.Conditions.Fatigued);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Fatigued))
                    _List.Remove(Enumerations.Conditions.Fatigued);
            }
            if (chkFrightened.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Frightened))
                    _List.Add(Enumerations.Conditions.Frightened);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Frightened))
                    _List.Remove(Enumerations.Conditions.Frightened);
            }
            if (chkGrappled.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Grappled))
                    _List.Add(Enumerations.Conditions.Grappled);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Grappled))
                    _List.Remove(Enumerations.Conditions.Grappled);
            }
            if (chkIncapacitated.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Incapacitated))
                    _List.Add(Enumerations.Conditions.Incapacitated);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Incapacitated))
                    _List.Remove(Enumerations.Conditions.Incapacitated);
            }
            if (chkInvisible.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Invisible))
                    _List.Add(Enumerations.Conditions.Invisible);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Invisible))
                    _List.Remove(Enumerations.Conditions.Invisible);
            }
            if (chkParalyzed.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Paralyzed))
                    _List.Add(Enumerations.Conditions.Paralyzed);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Paralyzed))
                    _List.Remove(Enumerations.Conditions.Paralyzed);
            }
            if (chkPetrified.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Petrified))
                    _List.Add(Enumerations.Conditions.Petrified);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Petrified))
                    _List.Remove(Enumerations.Conditions.Petrified);
            }
            if (chkPoisoned.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Poisoned))
                    _List.Add(Enumerations.Conditions.Poisoned);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Poisoned))
                    _List.Remove(Enumerations.Conditions.Poisoned);
            }
            if (chkProne.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Prone))
                    _List.Add(Enumerations.Conditions.Prone);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Prone))
                    _List.Remove(Enumerations.Conditions.Prone);
            }
            if (chkRestrained.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Restrained))
                    _List.Add(Enumerations.Conditions.Restrained);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Restrained))
                    _List.Remove(Enumerations.Conditions.Restrained);
            }
            if (chkStunned.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Stunned))
                    _List.Add(Enumerations.Conditions.Stunned);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Stunned))
                    _List.Remove(Enumerations.Conditions.Stunned);
            }
            if (chkUnconscious.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.Conditions.Unconscious))
                    _List.Add(Enumerations.Conditions.Unconscious);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.Conditions.Unconscious))
                    _List.Remove(Enumerations.Conditions.Unconscious);
            }
            OnClose(true);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            OnClose(false);
        }
    }
}
