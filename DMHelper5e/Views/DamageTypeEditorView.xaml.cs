﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DnD5eData.Tools;
using DnD5eData.Models;
using DMHelper5e.Interfaces;
namespace DMHelper5e.Views
{
    /// <summary>
    /// Interaction logic for DamageTypeEditorView.xaml
    /// </summary>
    public partial class DamageTypeEditorView : UserControl, ICloseable
    {
        private List<Enumerations.DamageTypes> _List = new List<Enumerations.DamageTypes>();

        public event ClosingEventHandler OnClose;

        public List<Enumerations.DamageTypes> List { get { { return _List; } } }
        public DamageTypeEditorView(List<Enumerations.DamageTypes> list)
        {
            InitializeComponent();
            _List = list;
            chkAcid.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Acid);
            chkBludgeoning.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Bludgeoning);
            chkCold.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Cold);
            chkFire.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Fire);
            chkForce.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Force);
            chkLightning.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Lightning);
            chkNecrotic.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Necrotic);
            chkPiercing.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Piercing);
            chkPoison.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Poison);
            chkPsychic.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Psychic);
            chkRadiant.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Radiant);
            chkSlashing.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Slashing);
            chkThunder.IsChecked = _List.Any(p => p == Enumerations.DamageTypes.Thunder);
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (chkAcid.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Acid))
                    _List.Add(Enumerations.DamageTypes.Acid);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Acid))
                    _List.Remove(Enumerations.DamageTypes.Acid);
            }
            if (chkBludgeoning.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Bludgeoning))
                    _List.Add(Enumerations.DamageTypes.Bludgeoning);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Bludgeoning))
                    _List.Remove(Enumerations.DamageTypes.Bludgeoning);
            }
            if (chkCold.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Cold))
                    _List.Add(Enumerations.DamageTypes.Cold);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Cold))
                    _List.Remove(Enumerations.DamageTypes.Cold);
            }
            if (chkFire.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Fire))
                    _List.Add(Enumerations.DamageTypes.Fire);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Fire))
                    _List.Remove(Enumerations.DamageTypes.Fire);
            }
            if (chkForce.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Force))
                    _List.Add(Enumerations.DamageTypes.Force);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Force))
                    _List.Remove(Enumerations.DamageTypes.Force);
            }
            if (chkLightning.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Lightning))
                    _List.Add(Enumerations.DamageTypes.Lightning);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Lightning))
                    _List.Remove(Enumerations.DamageTypes.Lightning);
            }
            if (chkNecrotic.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Necrotic))
                    _List.Add(Enumerations.DamageTypes.Necrotic);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Necrotic))
                    _List.Remove(Enumerations.DamageTypes.Necrotic);
            }
            if (chkPiercing.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Piercing))
                    _List.Add(Enumerations.DamageTypes.Piercing);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Piercing))
                    _List.Remove(Enumerations.DamageTypes.Piercing);
            }
            if (chkPoison.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Poison))
                    _List.Add(Enumerations.DamageTypes.Poison);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Poison))
                    _List.Remove(Enumerations.DamageTypes.Poison);
            }
            if (chkPsychic.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Psychic))
                    _List.Add(Enumerations.DamageTypes.Psychic);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Psychic))
                    _List.Remove(Enumerations.DamageTypes.Psychic);
            }
            if (chkRadiant.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Radiant))
                    _List.Add(Enumerations.DamageTypes.Radiant);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Radiant))
                    _List.Remove(Enumerations.DamageTypes.Radiant);
            }
            if (chkSlashing.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Slashing))
                    _List.Add(Enumerations.DamageTypes.Slashing);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Slashing))
                    _List.Remove(Enumerations.DamageTypes.Slashing);
            }
            if (chkThunder.IsChecked.Value)
            {
                if (!_List.Any(p => p == Enumerations.DamageTypes.Thunder))
                    _List.Add(Enumerations.DamageTypes.Thunder);
            }
            else
            {
                if (_List.Any(p => p == Enumerations.DamageTypes.Thunder))
                    _List.Remove(Enumerations.DamageTypes.Thunder);
            }
            OnClose(true);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            OnClose(false);
        }
    }
}
