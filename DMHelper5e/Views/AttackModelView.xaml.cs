﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DnD5eData.Tools;
using DnD5eData.Models;
using DMHelper5e.Interfaces;
using DMHelper5e.ViewModels;
namespace DMHelper5e.Views
{
    /// <summary>
    /// Interaction logic for AttackModelView.xaml
    /// </summary>
    public partial class AttackModelView : UserControl
    {
        public AttackModelView(AttackModel model)
        {
            InitializeComponent();
            DataContext = new AttackModelViewModel(model);
        }
    }
}
