﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DMHelper5e.ViewModels;
using DMHelper5e.Interfaces;
using DnD5eData.DataAccess;
using DnD5eData.DataAccess.Requests;
using DnD5eData.DataAccess.Responses;
namespace DMHelper5e.Views
{
    /// <summary>
    /// Interaction logic for CreatureEditorView.xaml
    /// </summary>
    public partial class CreatureEditorView : UserControl , ICloseable
    {
        private CreatureEditorViewModel _ViewModel;
        private Window _Owner;
        private bool _InEditMode;

        public event ClosingEventHandler OnClose;

        public CreatureEditorViewModel ViewModel {  get { return _ViewModel; } set { _ViewModel = value; } }
        public CreatureEditorView(CreatureEditorViewModel viewModel, Window owner, bool isNew)
        {
            _ViewModel = viewModel;
            InitializeComponent();
            DataContext = _ViewModel;
            _Owner = owner;
            _InEditMode = !isNew;
            LoadFeatures();
            LoadActions();
            LoadReactions();
            LoadAttacks();
            LoadWeapons();
        }
        private void LoadFeatures()
        {
            stackFeatures.Children.Clear();
            foreach (var feat in _ViewModel.Model.Features)
            {
                stackFeatures.Children.Add(new BaseModelView(feat));
            }
        }
        private void LoadActions()
        {
            stackActions.Children.Clear();
            foreach (var action in _ViewModel.Model.Actions)
            {
                stackActions.Children.Add(new BaseModelView(action));
            }
        }
        private void LoadReactions()
        {
            stackReactions.Children.Clear();
            foreach (var reaction in _ViewModel.Model.Reactions)
            {
                stackReactions.Children.Add(new BaseModelView(reaction));
            }
        }
        private void LoadAttacks()
        {
            stackAttacks.Children.Clear();
            foreach (var attack in _ViewModel.Model.Attacks)
            {
                stackAttacks.Children.Add(new AttackModelView(attack));
            }
        }
        private void LoadWeapons()
        {
            stackWeapons.Children.Clear();
            foreach (var weapon in _ViewModel.Model.Weapons)
            {
                stackWeapons.Children.Add(new WeaponModelView(weapon));
            }
        }
        private void btnArmor_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnConditionVunerabilities_Click(object sender, RoutedEventArgs e)
        {
            ConditionTypeEditorWindow editor = new ConditionTypeEditorWindow(_ViewModel.GetConditionVunerabilities(), DnD5eData.Tools.Enumerations.Editors.Vunerabilities);
            editor.Owner = _Owner;
            editor.ShowDialog();
            if (editor.Saved) _ViewModel.SetConditionVunerabilities(editor.List);
        }

        private void btnDamageVunerabilities_Click(object sender, RoutedEventArgs e)
        {
            DamageTypeEditorWindow editor = new DamageTypeEditorWindow(_ViewModel.GetDamageVunerabilities(), DnD5eData.Tools.Enumerations.Editors.Vunerabilities);
            editor.Owner = _Owner;
            editor.ShowDialog();
            if (editor.Saved) _ViewModel.SetDamageVunerabilities(editor.List);
        }

        private void btnConditionImmunities_Click(object sender, RoutedEventArgs e)
        {
            ConditionTypeEditorWindow editor = new ConditionTypeEditorWindow(_ViewModel.GetConditionImmunities(), DnD5eData.Tools.Enumerations.Editors.Immunities);
            editor.Owner = _Owner;
            editor.ShowDialog();
            if (editor.Saved) _ViewModel.SetConditionImmunities(editor.List);
        }

        private void btnDamageImmunities_Click(object sender, RoutedEventArgs e)
        {
            DamageTypeEditorWindow editor = new DamageTypeEditorWindow(_ViewModel.GetDamageImmunities(), DnD5eData.Tools.Enumerations.Editors.Immunities);
            editor.Owner = _Owner;
            editor.ShowDialog();
            if (editor.Saved) _ViewModel.SetDamageImmunities(editor.List);
        }

        private void btnConditionResistances_Click(object sender, RoutedEventArgs e)
        {
            ConditionTypeEditorWindow editor = new ConditionTypeEditorWindow(_ViewModel.GetConditionResistances(), DnD5eData.Tools.Enumerations.Editors.Resistance);
            editor.Owner = _Owner;
            editor.ShowDialog();
            if (editor.Saved) _ViewModel.SetConditionResistances(editor.List);
        }

        private void btnDamageResistances_Click(object sender, RoutedEventArgs e)
        {
            DamageTypeEditorWindow editor = new DamageTypeEditorWindow(_ViewModel.GetDamageResistances(), DnD5eData.Tools.Enumerations.Editors.Resistance);
            editor.Owner = _Owner;
            editor.ShowDialog();
            if (editor.Saved) _ViewModel.SetDamageResistances(editor.List);
        }

        private void btnLanguages_Click(object sender, RoutedEventArgs e)
        {
            LanguageEditorWindow editor = new LanguageEditorWindow(_ViewModel.GetLanguages());
            editor.Owner = _Owner;
            editor.ShowDialog();
            if (editor.Saved) _ViewModel.SetLanguages(editor.List);
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (_InEditMode)
            {
                UpdateMonsterRequest request = UpdateMonsterRequest.CreateNew();
                request.Requests.Add(_ViewModel.Model);
                UpdateMonsterResponse response = DatabaseAccess.Instance.UpdateMonster(request);
                OnClose(response.Success);
            }
            else
            {
                CreateMonsterRequest request = CreateMonsterRequest.CreateNew();
                request.Requests.Add(_ViewModel.Model);
                CreateMonsterResponse response = DatabaseAccess.Instance.CreateMonster(request);
                OnClose(response.Success);
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            OnClose(false);
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _ViewModel.Strength = _ViewModel.Strength;
            _ViewModel.Dexterity = _ViewModel.Dexterity;
            _ViewModel.Constitution = _ViewModel.Constitution;
            _ViewModel.Intelligence = _ViewModel.Intelligence;
            _ViewModel.Wisdom = _ViewModel.Wisdom;
            _ViewModel.Charisma = _ViewModel.Charisma;
        }
    }
}
