﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DnD5eData.DataAccess;
using DnD5eData.DataAccess.Requests;
using DnD5eData.DataAccess.Responses;
using DnD5eData.Models;
using DnD5eData.Tools;
using System.Collections.Generic;
namespace DnD5eData.Tests
{
    [TestClass]
    public class WeaponTests
    {
        [TestMethod]
        public void CreateTest()
        {
            List<WeaponModel> weapons = new List<WeaponModel>();
            weapons.Add(new WeaponModel()
            {
                AttackBonus = 0,
                DamageBonus = 0,
                DamageType = Enumerations.DamageTypes.Piercing,
                Description = "Finesse, light, thrown (range 20/60)",
                Die = Enumerations.Dice.d4,
                Id = 0,
                IsTwoHanded = false,
                IsVersitile = false,
                Name = "Dagger",
                NumberOfDice = 1,
                WeaponClass = Enumerations.WeaponClasses.SimpleMelee
            });
            weapons.Add(new WeaponModel()
            {
                AttackBonus = 0,
                DamageBonus = 0,
                DamageType = Enumerations.DamageTypes.Bludgeoning,
                Description = "Two-handed",
                Die = Enumerations.Dice.d4,
                Id = 0,
                IsTwoHanded = true,
                IsVersitile = false,
                Name = "GreatClub",
                NumberOfDice = 1,
                WeaponClass = Enumerations.WeaponClasses.SimpleMelee
            });
            CreateWeaponRequest request = CreateWeaponRequest.CreateNew(weapons);
            CreateWeaponResponse response = DatabaseAccess.Instance.CreateWeapon(request);
            Assert.IsTrue(response.Success);
            Assert.IsNull(response.Error);
        }
    }
}
