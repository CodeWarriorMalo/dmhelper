﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using DnD5eData.Models;
using DnD5eData.DataAccess.Responses;
using DnD5eData.DataAccess.Requests;
using DnD5eData.Tools;
namespace DnD5eData.DataAccess.Interfaces
{
    internal class BaseInterface<DataType>
    {
        protected string _PathToFiles { get; set; }
        protected string _FileName { get; set; }
        protected List<DataType> _List { get; set; }
        private bool PathExists()
        {
#if DEBUG
            string dir = @"C:\Projects\dmhelper\DnD5eData\";
#else
            string dir = Directory.GetCurrentDirectory();
#endif
            if (_PathToFiles.Contains(dir))
                return Directory.Exists(_PathToFiles);
            _PathToFiles = dir + _PathToFiles.Replace("/", "\\");
            return Directory.Exists(_PathToFiles);
        }
        private bool FileExists()
        {
            return File.Exists(_PathToFiles + _FileName);
        }
        public bool LoadData()
        {
            try
            {
                if (!PathExists()) return false;
                if (FileExists())
                {
                    _List = Serializer<List<DataType>>.DeserializeFromFile(_PathToFiles + _FileName);
                }
                if (_List == null) _List = new List<DataType>();
            }
            catch
            {
                return false;
            }
            return true;
        }
        public bool SaveData()
        {
            try
            {
                if (!PathExists()) return false;
                return Serializer<List<DataType>>.SerializeToFile(_List, _PathToFiles + _FileName);
            }
            catch
            {
                return false;
            }
            return true;
        }
    }
}
