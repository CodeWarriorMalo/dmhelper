﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
using DnD5eData.DataAccess.Responses;
using DnD5eData.DataAccess.Requests;
using System.Configuration;


namespace DnD5eData.DataAccess.Interfaces
{
    internal class PlayerInterface : BaseInterface<CreatureModel>
    {
        public PlayerInterface()
        {
            _PathToFiles = ConfigurationManager.AppSettings["PathToData"];
            _FileName = ConfigurationManager.AppSettings["PlayerData"];
        }
        #region CreatePlayer
        public CreatePlayerResponse CreatePlayer(CreatePlayerRequest request)
        {
            CreatePlayerResponse response = CreatePlayerResponse.CreateNew();
            try
            {
                if (LoadData())
                {
                    foreach (var req in request.Requests)
                    {
                        if (string.IsNullOrWhiteSpace(req.Name))
                        { if (_List.Any(p => p.Id == req.Id)) throw new Exception(string.Format("{0} Already Exists", req.Id)); }
                        else
                        { if (_List.Any(p => p.Name == req.Name)) throw new Exception(string.Format("{0} Already Exists", req.Name)); }
                        if (_List.Count > 0)
                            req.Id = _List.OrderByDescending(p => p.Id).First().Id + 1;
                        else
                            req.Id = 1;
                        _List.Add(req);
                    }
                    if (!SaveData()) throw new Exception("Failed To Save File");
                }
                else
                {
                    throw new Exception(string.Format("File \"{0}\" Not Found.", _PathToFiles + _FileName));
                }
            }
            catch (Exception ex)
            {
                response.Error = ex;
            }
            finally
            {
                response.Success = response.Error == null;
            }
            return response;
        }
        #endregion
        #region RetrievePlayer
        public RetrievePlayerResponse RetrievePlayer(RetrievePlayerRequest request)
        {
            RetrievePlayerResponse response = RetrievePlayerResponse.CreateNew();
            try
            {
                if (LoadData())
                {
                    if (request.Requests.Count == 0) response.Players = _List;
                    foreach (var req in request.Requests)
                    {
                        if (req.Id < 1 && string.IsNullOrWhiteSpace(req.Name))
                        {
                            response.Players = _List;
                            break;
                        }
                        if (string.IsNullOrWhiteSpace(req.Name))
                        { if (_List.Any(p => p.Id == req.Id)) response.Players.Add(_List.First(p => p.Id == req.Id)); }
                        else
                        { if (_List.Any(p => p.Name == req.Name)) response.Players.Add(_List.First(p => p.Name == req.Name)); }
                    }
                }
                else
                {
                    throw new Exception(string.Format("File \"{0}\" Not Found.", _PathToFiles + _FileName));
                }
            }
            catch (Exception ex)
            {
                response.Error = ex;
            }
            finally
            {
                response.Success = response.Error == null;
            }
            return response;
        }
        #endregion
        #region UpdatePlayer
        public UpdatePlayerResponse UpdatePlayer(UpdatePlayerRequest request)
        {
            UpdatePlayerResponse response = UpdatePlayerResponse.CreateNew();
            try
            {
                if (LoadData())
                {
                    foreach (var req in request.Requests)
                    {
                        if (!_List.Any(p => p.Id == req.Id)) throw new Exception(string.Format("{0} Does Not Exist", req.Id));
                        else
                        {
                            var updater = _List.First(p => p.Id == req.Id);
                            updater.Actions = req.Actions;
                            updater.Alignment = req.Alignment;
                            updater.Armor = req.Armor;
                            updater.ArmorClass = req.ArmorClass;
                            updater.Attacks = req.Attacks;
                            updater.Attributes = req.Attributes;
                            updater.ChallengeRating = req.ChallengeRating;
                            updater.ConditionImmunities = req.ConditionImmunities;
                            updater.ConditionResistances = req.ConditionResistances;
                            updater.ConditionVunerabilities = req.ConditionVunerabilities;
                            updater.CreatureType = req.CreatureType;
                            updater.CurrentArmorClass = req.CurrentArmorClass;
                            updater.CurrentConditions = req.CurrentConditions;
                            updater.CurrentHitPoints = req.CurrentHitPoints;
                            updater.CurrentInitiative = req.CurrentInitiative;
                            updater.CurrentSpeeds = req.CurrentSpeeds;
                            updater.DamageImmunities = req.DamageImmunities;
                            updater.DamageResistances = req.DamageResistances;
                            updater.DamageVunerabilities = req.DamageVunerabilities;
                            updater.DeathSaveFail = req.DeathSaveFail;
                            updater.DeathSaveSuccess = req.DeathSaveSuccess;
                            updater.Description = req.Description;
                            updater.ExperiencePoints = req.ExperiencePoints;
                            updater.Features = req.Features;
                            updater.HitPointCalc = req.HitPointCalc;
                            updater.HitPoints = req.HitPoints;
                            updater.Initiative = req.Initiative;
                            updater.IsAlive = req.IsAlive;
                            updater.Languages = req.Languages;
                            updater.Level = req.Level;
                            updater.Name = req.Name;
                            updater.PlayerType = req.PlayerType;
                            updater.ProficiencyBonus = req.ProficiencyBonus;
                            updater.Reactions = req.Reactions;
                            updater.SavingThrows = req.SavingThrows;
                            updater.Senses = req.Senses;
                            updater.Side = req.Side;
                            updater.Size = req.Size;
                            updater.Skills = req.Skills;
                            updater.Speeds = req.Speeds;
                            updater.SpellCasting = req.SpellCasting;
                            updater.TemporaryHitPoints = req.TemporaryHitPoints;
                            updater.Value = req.Value;
                            updater.Weapons = req.Weapons;
                        }
                    }
                    if (!SaveData()) throw new Exception("Failed To Save File");
                }
                else
                {
                    throw new Exception(string.Format("File \"{0}\" Not Found.", _PathToFiles + _FileName));
                }
            }
            catch (Exception ex)
            {
                response.Error = ex;
            }
            finally
            {
                response.Success = response.Error == null;
            }
            return response;
        }
        #endregion
        #region DeletePlayer
        public DeletePlayerResponse DeletePlayer(DeletePlayerRequest request)
        {
            DeletePlayerResponse response = DeletePlayerResponse.CreateNew();
            try
            {
                if (LoadData())
                {
                    foreach (var req in request.Requests)
                    {
                        if (!_List.Any(p => p.Id == req.Id)) throw new Exception(string.Format("{0} Does Not Exist", req.Id));
                        else
                        {
                            _List.RemoveAll(p => p.Id == req.Id);
                        }
                    }
                    if (!SaveData()) throw new Exception("Failed To Save File");
                }
                else
                {
                    throw new Exception(string.Format("File \"{0}\" Not Found.", _PathToFiles + _FileName));
                }
            }
            catch (Exception ex)
            {
                response.Error = ex;
            }
            finally
            {
                response.Success = response.Error == null;
            }
            return response;
        }
        #endregion
    }
}
