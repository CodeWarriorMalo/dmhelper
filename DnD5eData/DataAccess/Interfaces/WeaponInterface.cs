﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
using DnD5eData.DataAccess.Responses;
using DnD5eData.DataAccess.Requests;
using System.Configuration;
namespace DnD5eData.DataAccess.Interfaces
{
    internal class WeaponInterface : BaseInterface<WeaponModel>
    {
        public WeaponInterface()
        {
            _PathToFiles = ConfigurationManager.AppSettings["PathToData"];
            _FileName = ConfigurationManager.AppSettings["WeaponsData"];
        }
        #region CreateWeapon
        public CreateWeaponResponse CreateWeapon(CreateWeaponRequest request)
        {
            CreateWeaponResponse response = CreateWeaponResponse.CreateNew();
            try
            {
                if (LoadData())
                {
                    foreach (var req in request.Requests)
                    {
                        if (string.IsNullOrWhiteSpace(req.Name))
                        { if (_List.Any(p => p.Id == req.Id)) throw new Exception(string.Format("{0} Already Exists", req.Id)); }
                        else
                        { if (_List.Any(p => p.Name == req.Name)) throw new Exception(string.Format("{0} Already Exists", req.Name)); }
                        if (_List.Count > 0)
                            req.Id = _List.OrderByDescending(p => p.Id).First().Id + 1;
                        else
                            req.Id = 1;
                        _List.Add(req);
                    }
                    if (!SaveData()) throw new Exception("Failed To Save File");
                }
                else
                {
                    throw new Exception(string.Format("File \"{0}\" Not Found.", _PathToFiles + _FileName));
                }
            }
            catch (Exception ex)
            {
                response.Error = ex;
            }
            finally
            {
                response.Success = response.Error == null;
            }
            return response;
        }
        #endregion
        #region RetrieveWeapon
        public RetrieveWeaponResponse RetrieveWeapon(RetrieveWeaponRequest request)
        {
            RetrieveWeaponResponse response = RetrieveWeaponResponse.CreateNew();
            try
            {
                if (LoadData())
                {
                    if (request.Requests.Count == 0) response.Weapons = _List;
                    foreach (var req in request.Requests)
                    {
                        if(req.Id < 1 && string.IsNullOrWhiteSpace(req.Name))
                        {
                            response.Weapons = _List;
                            break;
                        }
                        if (string.IsNullOrWhiteSpace(req.Name))
                        { if (_List.Any(p => p.Id == req.Id)) response.Weapons.Add(_List.First(p => p.Id == req.Id)); }
                        else
                        { if (_List.Any(p => p.Name == req.Name)) response.Weapons.Add(_List.First(p => p.Name == req.Name)); }
                    }
                }
                else
                {
                    throw new Exception(string.Format("File \"{0}\" Not Found.", _PathToFiles + _FileName));
                }
            }
            catch (Exception ex)
            {
                response.Error = ex;
            }
            finally
            {
                response.Success = response.Error == null;
            }
            return response;
        }
        #endregion
        #region UpdateWeapon
        public UpdateWeaponResponse UpdateWeapon(UpdateWeaponRequest request)
        {
            UpdateWeaponResponse response = UpdateWeaponResponse.CreateNew();
            try
            {
                if (LoadData())
                {
                    foreach (var req in request.Requests)
                    {
                        if (!_List.Any(p => p.Id == req.Id)) throw new Exception(string.Format("{0} Does Not Exist", req.Id));
                        else
                        {
                            var updater = _List.First(p => p.Id == req.Id);
                            updater.AttackBonus = req.AttackBonus;
                            updater.DamageBonus = req.DamageBonus;
                            updater.DamageType = req.DamageType;
                            updater.Description = req.Description;
                            updater.Die = req.Die;
                            updater.IsTwoHanded = req.IsTwoHanded;
                            updater.IsVersitile = req.IsVersitile;
                            updater.Name = req.Name;
                            updater.NumberOfDice = req.NumberOfDice;
                            updater.WeaponClass = req.WeaponClass;
                        }
                    }
                    if (!SaveData()) throw new Exception("Failed To Save File");
                }
                else
                {
                    throw new Exception(string.Format("File \"{0}\" Not Found.", _PathToFiles + _FileName));
                }
            }
            catch (Exception ex)
            {
                response.Error = ex;
            }
            finally
            {
                response.Success = response.Error == null;
            }
            return response;
        }
        #endregion
        #region DeleteWeapon
        public DeleteWeaponResponse DeleteWeapon(DeleteWeaponRequest request)
        {
            DeleteWeaponResponse response = DeleteWeaponResponse.CreateNew();
            try
            {
                if (LoadData())
                {
                    foreach (var req in request.Requests)
                    {
                        if (!_List.Any(p => p.Id == req.Id)) throw new Exception(string.Format("{0} Does Not Exist", req.Id));
                        else
                        {
                             _List.RemoveAll(p => p.Id == req.Id);
                        }
                    }
                    if (!SaveData()) throw new Exception("Failed To Save File");
                }
                else
                {
                    throw new Exception(string.Format("File \"{0}\" Not Found.", _PathToFiles + _FileName));
                }
            }
            catch (Exception ex)
            {
                response.Error = ex;
            }
            finally
            {
                response.Success = response.Error == null;
            }
            return response;
        }
        #endregion
    }
}
