﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Requests
{
    public class DeleteMonsterRequest : CreateMonsterRequest
    {
        public static new DeleteMonsterRequest CreateNew()
        {
            DeleteMonsterRequest request = new DeleteMonsterRequest();
            request.Requests = new List<Models.CreatureModel>();
            return request;
        }
    }
}
