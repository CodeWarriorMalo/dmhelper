﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Requests
{
    public class DeletePlayerRequest : CreatePlayerRequest
    {
        public static new DeletePlayerRequest CreateNew()
        {
            DeletePlayerRequest request = new DeletePlayerRequest();
            return request;
        }
    }
}