﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
namespace DnD5eData.DataAccess.Requests
{
    public class CreateWeaponRequest
    {
        public List<WeaponModel> Requests { get; set; }
        public static CreateWeaponRequest CreateNew()
        {
            CreateWeaponRequest request = new CreateWeaponRequest();
            request.Requests = new List<WeaponModel>();
            return request;
        }
        public static CreateWeaponRequest CreateNew(List<WeaponModel> requests)
        {
            CreateWeaponRequest request = new CreateWeaponRequest();
            request.Requests = requests;
            return request;
        }
    }
}
