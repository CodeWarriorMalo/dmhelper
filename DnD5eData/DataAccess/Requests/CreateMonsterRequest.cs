﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
namespace DnD5eData.DataAccess.Requests
{
    public class CreateMonsterRequest
    {
        public List<CreatureModel> Requests { get; set; }
        public static CreateMonsterRequest CreateNew()
        {
            CreateMonsterRequest request = new CreateMonsterRequest();
            request.Requests = new List<CreatureModel>();
            return request;
        }
        public static CreateMonsterRequest CreateNew(List<CreatureModel> requests)
        {
            CreateMonsterRequest request = new CreateMonsterRequest();
            request.Requests = requests;
            return request;
        }
    }
}