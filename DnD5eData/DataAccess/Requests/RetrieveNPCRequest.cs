﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Requests
{
    public class RetrieveNPCRequest
    {
        public List<BaseRequest> Requests { get; set; }
        public static RetrieveNPCRequest CreateNew()
        {
            RetrieveNPCRequest request = new RetrieveNPCRequest();
            request.Requests = new List<BaseRequest>();
            return request;
        }
    }
}
