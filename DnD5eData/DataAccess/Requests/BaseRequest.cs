﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Requests
{
    public class BaseRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
