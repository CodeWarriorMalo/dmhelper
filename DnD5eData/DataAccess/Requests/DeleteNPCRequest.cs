﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Requests
{
    public class DeleteNPCRequest : CreateNPCRequest
    {
        public static new DeleteNPCRequest CreateNew()
        {
            DeleteNPCRequest request = new DeleteNPCRequest();
            return request;
        }
    }
}
