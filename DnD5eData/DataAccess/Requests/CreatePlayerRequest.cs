﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
namespace DnD5eData.DataAccess.Requests
{
    public class CreatePlayerRequest
    {
        public List<CreatureModel> Requests { get; set; }
        public static CreatePlayerRequest CreateNew()
        {
            CreatePlayerRequest request = new CreatePlayerRequest();
            request.Requests = new List<CreatureModel>();
            return request;
        }
        public static CreatePlayerRequest CreateNew(List<CreatureModel> requests)
        {
            CreatePlayerRequest request = new CreatePlayerRequest();
            request.Requests = requests;
            return request;
        }
    }
}