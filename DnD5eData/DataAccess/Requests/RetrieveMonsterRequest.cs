﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Requests
{
    public class RetrieveMonsterRequest
    {
        public List<BaseRequest> Requests { get; set; }
        public static RetrieveMonsterRequest CreateNew()
        {
            RetrieveMonsterRequest request = new RetrieveMonsterRequest();
            request.Requests = new List<BaseRequest>();
            return request;
        }
    }
}
