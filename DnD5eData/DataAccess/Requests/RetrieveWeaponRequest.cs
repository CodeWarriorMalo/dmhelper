﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Requests
{
    public class RetrieveWeaponRequest
    {
        public List<BaseRequest> Requests { get; set; }
        public static RetrieveWeaponRequest CreateNew()
        {
            RetrieveWeaponRequest request = new RetrieveWeaponRequest();
            request.Requests = new List<BaseRequest>();
            return request;
        }
    }
}
