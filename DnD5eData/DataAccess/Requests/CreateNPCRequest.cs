﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
namespace DnD5eData.DataAccess.Requests
{
    public class CreateNPCRequest
    {
        public List<CreatureModel> Requests { get; set; }
        public static CreateNPCRequest CreateNew()
        {
            CreateNPCRequest request = new CreateNPCRequest();
            request.Requests = new List<CreatureModel>();
            return request;
        }
        public static CreateNPCRequest CreateNew(List<CreatureModel> requests)
        {
            CreateNPCRequest request = new CreateNPCRequest();
            request.Requests = requests;
            return request;
        }
    }
}