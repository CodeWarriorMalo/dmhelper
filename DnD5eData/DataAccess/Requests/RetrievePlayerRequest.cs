﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Requests
{
    public class RetrievePlayerRequest
    {
        public List<BaseRequest> Requests { get; set; }
        public static RetrievePlayerRequest CreateNew()
        {
            RetrievePlayerRequest request = new RetrievePlayerRequest();
            request.Requests = new List<BaseRequest>();
            return request;
        }
    }
}
