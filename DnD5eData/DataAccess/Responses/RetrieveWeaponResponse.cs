﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
namespace DnD5eData.DataAccess.Responses
{
    public class RetrieveWeaponResponse : BaseResponse
    {
        public List<WeaponModel> Weapons { get; set; }
        public static RetrieveWeaponResponse CreateNew()
        {
            RetrieveWeaponResponse response = new Responses.RetrieveWeaponResponse();
            response.Weapons = new List<WeaponModel>();
            return response;
        }
    }
}
