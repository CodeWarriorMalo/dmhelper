﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Responses
{
    public class BaseResponse
    {
        public bool Success { get; set; }
        public Exception Error { get; set; }
    }
}
