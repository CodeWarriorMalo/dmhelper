﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;

namespace DnD5eData.DataAccess.Responses
{
    public class RetrieveNPCResponse : BaseResponse
    {
        public List<CreatureModel> NPCs { get; set; }
        public static RetrieveNPCResponse CreateNew()
        {
            RetrieveNPCResponse response = new RetrieveNPCResponse();
            response.NPCs = new List<CreatureModel>();
            return response;
        }
    }
}
