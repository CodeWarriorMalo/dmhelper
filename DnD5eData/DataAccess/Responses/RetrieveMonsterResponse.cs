﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;

namespace DnD5eData.DataAccess.Responses
{
    public class RetrieveMonsterResponse : BaseResponse
    {
        public List<CreatureModel> Monsters { get; set; }
        public static RetrieveMonsterResponse CreateNew()
        {
            RetrieveMonsterResponse response = new RetrieveMonsterResponse();
            response.Monsters = new List<CreatureModel>();
            return response;
        }
    }
}
