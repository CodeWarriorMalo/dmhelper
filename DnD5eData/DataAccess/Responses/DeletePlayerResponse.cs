﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Responses
{
    public class DeletePlayerResponse : CreatePlayerResponse
    {
        public static new DeletePlayerResponse CreateNew()
        {
            DeletePlayerResponse response = new DeletePlayerResponse();
            return response;
        }
    }
}
