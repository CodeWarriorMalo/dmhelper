﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Responses
{
    public class DeleteMonsterResponse : CreateMonsterResponse
    {
        public static new DeleteMonsterResponse CreateNew()
        {
            DeleteMonsterResponse response = new DeleteMonsterResponse();
            return response;
        }
    }
}
