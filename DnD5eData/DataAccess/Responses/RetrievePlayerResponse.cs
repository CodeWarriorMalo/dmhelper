﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;

namespace DnD5eData.DataAccess.Responses
{
    public class RetrievePlayerResponse : BaseResponse
    {
        public List<CreatureModel> Players { get; set; }
        public static RetrievePlayerResponse CreateNew()
        {
            RetrievePlayerResponse response = new RetrievePlayerResponse();
            response.Players = new List<CreatureModel>();
            return response;
        }
    }
}
