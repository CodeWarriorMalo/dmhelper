﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.DataAccess.Responses
{
    public class DeleteWeaponResponse : CreateWeaponResponse
    {
        public static new DeleteWeaponResponse CreateNew()
        {
            DeleteWeaponResponse response = new DeleteWeaponResponse();
            return response;
        }
    }
}
