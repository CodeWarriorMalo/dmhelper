﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.DataAccess.Interfaces;
using DnD5eData.DataAccess.Requests;
using DnD5eData.DataAccess.Responses;
namespace DnD5eData.DataAccess
{
    public class DatabaseAccess
    {
        public static DatabaseAccess Instance = new DatabaseAccess();
        #region Weapon
        public CreateWeaponResponse CreateWeapon(CreateWeaponRequest request)
        {
            WeaponInterface Interface = new WeaponInterface();
            return Interface.CreateWeapon(request);
        }
        public RetrieveWeaponResponse RetrieveWeapon(RetrieveWeaponRequest request)
        {
            WeaponInterface Interface = new WeaponInterface();
            return Interface.RetrieveWeapon(request);
        }
        public UpdateWeaponResponse UpdateWeapon(UpdateWeaponRequest request)
        {
            WeaponInterface Interface = new WeaponInterface();
            return Interface.UpdateWeapon(request);
        }
        public DeleteWeaponResponse DeleteWeapon(DeleteWeaponRequest request)
        {
            WeaponInterface Interface = new WeaponInterface();
            return Interface.DeleteWeapon(request);
        }
        #endregion
        #region Monster
        public CreateMonsterResponse CreateMonster(CreateMonsterRequest request)
        {
            MonsterInterface Interface = new MonsterInterface();
            return Interface.CreateMonster(request);
        }
        public RetrieveMonsterResponse RetrieveMonster(RetrieveMonsterRequest request)
        {
            MonsterInterface Interface = new MonsterInterface();
            return Interface.RetrieveMonster(request);
        }
        public UpdateMonsterResponse UpdateMonster(UpdateMonsterRequest request)
        {
            MonsterInterface Interface = new MonsterInterface();
            return Interface.UpdateMonster(request);
        }
        public DeleteMonsterResponse DeleteMonster(DeleteMonsterRequest request)
        {
            MonsterInterface Interface = new MonsterInterface();
            return Interface.DeleteMonster(request);
        }
        #endregion
        #region Player
        public CreatePlayerResponse CreatePlayer(CreatePlayerRequest request)
        {
            PlayerInterface Interface = new PlayerInterface();
            return Interface.CreatePlayer(request);
        }
        public RetrievePlayerResponse RetrievePlayer(RetrievePlayerRequest request)
        {
            PlayerInterface Interface = new PlayerInterface();
            return Interface.RetrievePlayer(request);
        }
        public UpdatePlayerResponse UpdatePlayer(UpdatePlayerRequest request)
        {
            PlayerInterface Interface = new PlayerInterface();
            return Interface.UpdatePlayer(request);
        }
        public DeletePlayerResponse DeletePlayer(DeletePlayerRequest request)
        {
            PlayerInterface Interface = new PlayerInterface();
            return Interface.DeletePlayer(request);
        }
        #endregion
        #region NPC
        public CreateNPCResponse CreateNPC(CreateNPCRequest request)
        {
            NPCInterface Interface = new NPCInterface();
            return Interface.CreateNPC(request);
        }
        public RetrieveNPCResponse RetrieveNPC(RetrieveNPCRequest request)
        {
            NPCInterface Interface = new NPCInterface();
            return Interface.RetrieveNPC(request);
        }
        public UpdateNPCResponse UpdateNPC(UpdateNPCRequest request)
        {
            NPCInterface Interface = new NPCInterface();
            return Interface.UpdateNPC(request);
        }
        public DeleteNPCResponse DeleteNPC(DeleteNPCRequest request)
        {
            NPCInterface Interface = new NPCInterface();
            return Interface.DeleteNPC(request);
        }
        #endregion
    }
}
