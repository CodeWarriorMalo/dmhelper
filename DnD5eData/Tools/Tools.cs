﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Models;
using DnD5eData.DataAccess;
using DnD5eData.DataAccess.Requests;
using DnD5eData.DataAccess.Responses;
namespace DnD5eData.Tools
{
    public class Tools
    {
        public static string GetWeaponName(int weaponId)
        {
            RetrieveWeaponRequest request = RetrieveWeaponRequest.CreateNew();
            request.Requests.Add(new BaseRequest() { Id = weaponId });
            RetrieveWeaponResponse response = DatabaseAccess.Instance.RetrieveWeapon(request);
            if (response.Success && response.Weapons.Count > 0) return response.Weapons.First().Name;
            return string.Empty;
        }
        public static int GetWeaponId(string weaponName)
        {
            RetrieveWeaponRequest request = RetrieveWeaponRequest.CreateNew();
            request.Requests.Add(new BaseRequest() { Name = weaponName });
            RetrieveWeaponResponse response = DatabaseAccess.Instance.RetrieveWeapon(request);
            if (response.Success && response.Weapons.Count > 0) return response.Weapons.First().Id;
            return 0;
        }
        public static string FormatNumber(int numberIn)
        {
            string numberOut = string.Empty;
            string temp = numberIn.ToString();
            string revNumber = string.Empty;
            int count = 0;
            for (int t = temp.Length - 1; t > -1; t--)
            {
                revNumber += temp.Substring(t, 1);
                count++;
                if (count == 3)
                {
                    count = 0;
                    revNumber += ",";
                }
            }
            if (revNumber.EndsWith(",")) revNumber = revNumber.Substring(0, revNumber.Length - 1);
            temp = revNumber;
            for (int t = temp.Length - 1; t > -1; t--)
            {
                numberOut += revNumber.Substring(t, 1);
            }
            return numberOut;
        }
    }
}
