﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.Tools
{
    public class Enumerations
    {
        public enum CreatureSides
        {
            Neutral,
            Ally,
            Enemy
        }
        public enum Players
        {
            Player,
            NonPlayerCharacter,
            Monster
        }
        public enum DamageTypes
        {
            Acid,
            Bludgeoning,
            Cold,
            Fire,
            Force,
            Lightning,
            Necrotic,
            Piercing,
            Poison,
            Psychic,
            Radiant,
            Slashing,
            Thunder
        }
        public enum Conditions
        {
            Blinded,
            Charmed,
            Deafened,
            Fatigued,
            Frightened,
            Grappled,
            Incapacitated,
            Invisible,
            Paralyzed,
            Petrified,
            Poisoned,
            Prone,
            Restrained,
            Stunned,
            Unconscious,
            Exhaustion
        }
        public enum SavingThrows
        {
            Charisma,
            Constitution,
            Dexterity,
            Intelligence,
            Strength,
            Wisdom
        }
        public enum Skills
        {
            Acrobatics,
            AnimalHandling,
            Arcana,
            Athletics,
            Deception,
            History,
            Insight,
            Intimidation,
            Investigation,
            Medicine,
            Nature,
            Perception,
            Performance,
            Persuasion,
            Religion,
            SleightOfHand,
            Stealth,
            Survival,
            StrSaves,
            DexSaves,
            ConSaves,
            IntSaves,
            WisSaves,
            ChaSaves
        }
        public enum Scripts
        {
            Common,
            Draconic,
            Dwarvish,
            Celestial,
            Elvish,
            Infernal
        }
        public enum Languages
        {
            Common,
            Dwarvish,
            Elvish,
            Giant,
            Gnomish,
            Goblin,
            Halfling,
            Orc,
            Abyssal,
            Celestial,
            Draconic,
            DeepSpeech,
            Infernal,
            Primordial,
            Sylvan,
            Undercommon
        }
        public enum Attributes
        {
            Charisma,
            Constitution,
            Dexterity,
            Intelligence,
            Strength,
            Wisdom
        }
        public enum Abbreviations
        {
            AC,
            CHA,
            CON,
            CR,
            DEX,
            DC,
            INT,
            HP,
            M,
            MOVE,
            PP,
            S,
            STR,
            V,
            WIS,
            XP
        }
        public enum Sizes
        {
            Fine,
            Diminutive,
            Tiny,
            Small,
            Medium,
            Large,
            Huge,
            Gargantuan,
            Colossal
        }
        public enum Alignments
        {
            LawfulGood,
            LawfulNeutral,
            LawfulEvil,
            NeutralGood,
            TrueNeutral,
            NeutralEvil,
            ChaoticGood,
            ChaoticNeutral,
            ChaoticEvil
        }
        public enum Types
        {
            Aberration,
            Beast,
            Celestial,
            Construct,
            Dragon,
            Elemental,
            Fey,
            Fiend,
            Giant,
            Humanoid,
            Monstrosity,
            Ooze,
            Plant,
            Undead
        }
        public enum SpellTypes
        {
            Abjuration,
            Cantrip,
            Conjuration,
            Divination,
            Enchantment,
            Evocation,
            Illusion,
            Necromancy,
            Transmutation
        }
        public enum CastingTimes
        {
            Action,
            BonusAction,
            Minute,
            Round
        }
        public enum DurationTimes
        {
            Day,
            Hour,
            Instantaneous,
            Minute,
            Round
        }
        public enum SpellComponentTypes
        {
            Material,
            Somatic,
            Vocal
        }
        public enum Dice
        {
            d4,
            d6,
            d8,
            d10,
            d12,
            d20,
            d100
        }
        public enum WeaponClasses
        {
            Improvised,
            SimpleMelee,
            SimpleRanged,
            MartialMelee,
            MartialRanged
        }
        public enum MovementTypes
        {
            Burrow,
            Climb,
            Crawl,
            Dig,
            Fly,
            Run,
            Swim,
            Walk
        }
        public enum Editors
        {
            Resistance,
            Immunities,
            Vunerabilities
        }
    }
}