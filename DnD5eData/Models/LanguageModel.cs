﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class LanguageModel : BaseModel
    {
        public Enumerations.Languages Language { get; set; }
        public Enumerations.Scripts Script { get; set; }
    }
}
