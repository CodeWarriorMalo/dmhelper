﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class EncounterModel : BaseModel
    {
        public string ActivePlayer { get; set; }
        public List<CreatureModel> Creatures { get; set; }
        public int XP
        {
            get
            {
                int xp = 0;
                if (Creatures.Count == 0) return xp;
                foreach(var monster in Creatures.Where(c=>c.Side == Tools.Enumerations.CreatureSides.Enemy))
                {
                    xp += monster.Value;
                }
                return xp;
            }
        }
        public int XPEach
        {
            get
            {
                int xp = XP;
                if (Creatures.Any(c => c.Side == Tools.Enumerations.CreatureSides.Ally))
                    return XP / Creatures.Count(c => c.Side == Tools.Enumerations.CreatureSides.Ally);
                return 0;
            }
        }
        public bool HasBeenSaved { get; set; }
        public static EncounterModel CreateNew ()
        {
            EncounterModel model = new Models.EncounterModel();
            model.Creatures = new List<Models.CreatureModel>();
            return model;
        }
        public static EncounterModel Load(string fileSpec)
        {
            return Serializer<EncounterModel>.DeserializeFromFile(fileSpec);
        }
        public bool Save(string fileSpec)
        {
            this.HasBeenSaved = true;
            return Serializer<EncounterModel>.SerializeToFile(this, fileSpec);
        }
        public List<EncounterItemModel> EncounterItems
        {
            get
            {
                List<EncounterItemModel> items = new List<EncounterItemModel>();
                foreach(var creature in Creatures)
                {
                    items.Add(new EncounterItemModel() {
                        Conditions = creature.CurrentConditions,
                        CurrentAC = creature.CurrentArmorClass,
                        CurrentHP = creature.CurrentHitPoints,
                        CurrentInit = creature.CurrentInitiative,
                        IsActive = false,
                        IsAlive = creature.IsAlive,
                        Name = creature.Name,
                        Side = creature.Side,
                        TemporaryHP = creature.TemporaryHitPoints
                    });
                }
                return items;
            }
        }
    }
}
