﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.Models
{
    public class SpellSlotModel
    {
        public List<SpellModel> Spell { get; set; }
        public int Known { get; set; }
        public int Slots { get; set; }
        public int Used { get; set; }
        public int Remaining { get; set; }

        public static SpellSlotModel CreateNew()
        {
            SpellSlotModel model = new SpellSlotModel();
            model.Spell = new List<SpellModel>();
            model.Known = 0;
            model.Slots = 0;
            model.Used = 0;
            model.Remaining = 0;
            return model;
        }
    }
}
