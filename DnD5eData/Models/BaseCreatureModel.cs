﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class BaseCreatureModel : BaseModel
    {
        public Enumerations.Alignments Alignment { get; set; }
        public int ArmorClass { get; set; }
        public List<BaseModel> Actions { get; set; }
        public List<ArmorModel> Armor { get; set; }
        public List<AttackModel> Attacks { get; set; }
        public List<AttributeModel> Attributes { get; set; }
        public float ChallengeRating { get; set; }
        public List<Enumerations.Conditions> ConditionImmunities { get; set; }
        public List<Enumerations.Conditions> ConditionResistances { get; set; }
        public List<Enumerations.Conditions> ConditionVunerabilities { get; set; }
        public Enumerations.Types CreatureType { get; set; }
        public List<Enumerations.DamageTypes> DamageImmunities { get; set; }
        public List<Enumerations.DamageTypes> DamageResistances { get; set; }
        public List<Enumerations.DamageTypes> DamageVunerabilities { get; set; }
        public int ExperiencePoints { get; set; }
        public List<BaseModel> Features { get; set; }
        public int HitPoints { get; set; }
        public HitPointsModel HitPointCalc { get; set; }
        public int Initiative { get; set; }
        public List<LanguageModel> Languages { get; set; }
        public int Level { get; set; }
        public List<BaseModel> Reactions { get; set; }
        public List<SavingThrowModel> SavingThrows { get; set; }
        public string Senses { get; set; }
        public Enumerations.Sizes Size { get; set; }
        public List<SkillModel> Skills { get; set; }
        public List<SpeedModel> Speeds { get; set; }
        public SpellCastingModel SpellCasting { get; set; }
        public int Value { get; set; }
        public List<WeaponModel> Weapons { get; set; }
        public int ProficiencyBonus { get; set; }
    }
}
