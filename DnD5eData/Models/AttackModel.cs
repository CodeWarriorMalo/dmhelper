﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.Models
{
    public class AttackModel : BaseModel
    {
        public int AttackBonus { get; set; }
        public int WeaponId { get; set; }
        public int DamageBonus { get; set; }
    }
}
