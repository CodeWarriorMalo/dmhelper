﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class SpellModel : BaseModel
    {
        public int Level { get; set; }
        public Enumerations.SpellTypes SpellType { get; set; }
        public int CastingTimeAmount { get; set; }
        public Enumerations.CastingTimes CastingTime {get;set;}
        public string Range { get; set; }
        public List<Enumerations.SpellComponentTypes> Components { get; set; }
        public string Material { get; set; }
        public string Somatic { get; set; }
        public string Vocal { get; set; }
        public int DurationTimeAmount { get; set; }
        public Enumerations.DurationTimes DurationTime { get; set; }
        public bool CanBeRiturallyCast { get; set; }
        public bool RequiresConcentration { get; set; }
        public static SpellModel CreateNew()
        {
            SpellModel model = new SpellModel();
            model.Components = new List<Enumerations.SpellComponentTypes>();
            return model;
        }
    }
}
