﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class AttributeModel : BaseModel
    {
        public Enumerations.Attributes Attribute { get; set; }
        public Enumerations.Abbreviations Abbreviation { get; set; }
        public int Value { get; set; }
        public int Bonus { get; set; }
    }
}
