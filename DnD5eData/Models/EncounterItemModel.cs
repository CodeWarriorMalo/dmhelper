﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class EncounterItemModel 
    {
        public int Order { get; set; }
        public string Name { get; set; }
        public int CurrentInit { get; set; }
        public int CurrentHP { get; set; }
        public int TemporaryHP { get; set; }
        public int CurrentAC { get; set; }
        public Enumerations.CreatureSides Side { get; set; }
        public List<Enumerations.Conditions> Conditions { get; set; }
        public bool IsAlive { get; set; }
        public bool IsActive { get; set; }
        public static EncounterItemModel CreateNew()
        {
            EncounterItemModel model = new Models.EncounterItemModel();
            model.Conditions = new List<Tools.Enumerations.Conditions>();
            return model;
        }
    }
}
