﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class CreatureModel : BaseCreatureModel
    {
        public Enumerations.Players PlayerType { get; set; }
        public Enumerations.CreatureSides Side { get; set; }
        public int CurrentHitPoints { get; set; }
        public int TemporaryHitPoints { get; set; }
        public List<Enumerations.Conditions> CurrentConditions { get; set; }
        public int CurrentInitiative { get; set; }
        public int CurrentArmorClass { get; set; }
        public List<SpeedModel> CurrentSpeeds { get; set; }
        public bool IsAlive { get; set; }
        public int DeathSaveFail { get; set; }
        public int DeathSaveSuccess { get; set; }
        public void TakeDamage(int damage)
        {
            int currentHP = CurrentHitPoints;
            currentHP -= damage;
            if (currentHP < 0)
            {
                IsAlive = false;
                CurrentHitPoints = 0;
            }
            else CurrentHitPoints = currentHP;
        }
        public static CreatureModel CreateNew()
        {
            CreatureModel model = new CreatureModel();
            model.Actions = new List<BaseModel>();
            model.Alignment = Enumerations.Alignments.LawfulGood;
            model.Armor = new List<ArmorModel>();
            model.ArmorClass = 0;
            model.Attacks = new List<AttackModel>();
            model.Attributes = new List<AttributeModel>();
            model.Attributes.Add(new AttributeModel()
            {
                Abbreviation = Enumerations.Abbreviations.STR,
                Attribute = Enumerations.Attributes.Strength,
                Bonus = 0,
                Description = "Strength",
                Id = 1,
                Name = "Strength",
                Value = 10
            });
            model.Attributes.Add(new AttributeModel()
            {
                Abbreviation = Enumerations.Abbreviations.DEX,
                Attribute = Enumerations.Attributes.Dexterity,
                Bonus = 0,
                Description = "Dexterity",
                Id = 2,
                Name = "Dexterity",
                Value = 10
            });
            model.Attributes.Add(new AttributeModel()
            {
                Abbreviation = Enumerations.Abbreviations.CON,
                Attribute = Enumerations.Attributes.Constitution,
                Bonus = 0,
                Description = "Constitution",
                Id = 3,
                Name = "Constitution",
                Value = 10
            });
            model.Attributes.Add(new AttributeModel()
            {
                Abbreviation = Enumerations.Abbreviations.INT,
                Attribute = Enumerations.Attributes.Intelligence,
                Bonus = 0,
                Description = "Intelligence",
                Id = 4,
                Name = "Intelligence",
                Value = 10
            });
            model.Attributes.Add(new AttributeModel()
            {
                Abbreviation = Enumerations.Abbreviations.WIS,
                Attribute = Enumerations.Attributes.Wisdom,
                Bonus = 0,
                Description = "Wisdom",
                Id = 5,
                Name = "Wisdom",
                Value = 10
            });
            model.Attributes.Add(new AttributeModel()
            {
                Abbreviation = Enumerations.Abbreviations.CHA,
                Attribute = Enumerations.Attributes.Charisma,
                Bonus = 0,
                Description = "Charisma",
                Id = 6,
                Name = "Charisma",
                Value = 10
            });
            model.ChallengeRating = 0f;
            model.ConditionImmunities = new List<Enumerations.Conditions>();
            model.ConditionResistances = new List<Enumerations.Conditions>();
            model.ConditionVunerabilities = new List<Enumerations.Conditions>();
            model.CreatureType = Enumerations.Types.Aberration;
            model.CurrentArmorClass = 0;
            model.CurrentConditions = new List<Tools.Enumerations.Conditions>();
            model.CurrentHitPoints = 0;
            model.CurrentInitiative = 0;
            model.CurrentSpeeds = new List<SpeedModel>();
            model.DamageImmunities = new List<Enumerations.DamageTypes>();
            model.DamageResistances = new List<Enumerations.DamageTypes>();
            model.DamageVunerabilities = new List<Enumerations.DamageTypes>();
            model.Description = string.Empty;
            model.ExperiencePoints = 0;
            model.Features = new List<BaseModel>();
            model.HitPointCalc = new HitPointsModel();
            model.HitPoints = 0;
            model.Id = 0;
            model.Initiative = 0;
            model.Languages = new List<LanguageModel>();
            model.Level = 0;
            model.Name = string.Empty;
            model.PlayerType = Enumerations.Players.Monster;
            model.Reactions = new List<BaseModel>();
            model.SavingThrows = new List<SavingThrowModel>();
            model.Side = Enumerations.CreatureSides.Enemy;
            model.Size = Enumerations.Sizes.Medium;
            model.Skills = new List<SkillModel>();
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Acrobatics",
                Id = (int)Enumerations.Skills.Acrobatics,
                Name = "Acrobatics",
                Skill = Enumerations.Skills.Acrobatics,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "AnimalHandling",
                Id = (int)Enumerations.Skills.AnimalHandling,
                Name = "AnimalHandling",
                Skill = Enumerations.Skills.AnimalHandling,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Arcana",
                Id = (int)Enumerations.Skills.Arcana,
                Name = "Arcana",
                Skill = Enumerations.Skills.Arcana,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Athletics",
                Id = (int)Enumerations.Skills.Athletics,
                Name = "Athletics",
                Skill = Enumerations.Skills.Athletics,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Deception",
                Id = (int)Enumerations.Skills.Deception,
                Name = "Deception",
                Skill = Enumerations.Skills.Deception,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "History",
                Id = (int)Enumerations.Skills.History,
                Name = "History",
                Skill = Enumerations.Skills.History,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Insight",
                Id = (int)Enumerations.Skills.Insight,
                Name = "Insight",
                Skill = Enumerations.Skills.Insight,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Intimidation",
                Id = (int)Enumerations.Skills.Intimidation,
                Name = "Intimidation",
                Skill = Enumerations.Skills.Intimidation,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Investigation",
                Id = (int)Enumerations.Skills.Investigation,
                Name = "Investigation",
                Skill = Enumerations.Skills.Investigation,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Medicine",
                Id = (int)Enumerations.Skills.Medicine,
                Name = "Medicine",
                Skill = Enumerations.Skills.Medicine,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Nature",
                Id = (int)Enumerations.Skills.Nature,
                Name = "Nature",
                Skill = Enumerations.Skills.Nature,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Perception",
                Id = (int)Enumerations.Skills.Perception,
                Name = "Perception",
                Skill = Enumerations.Skills.Perception,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Performance",
                Id = (int)Enumerations.Skills.Performance,
                Name = "Performance",
                Skill = Enumerations.Skills.Performance,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Persuasion",
                Id = (int)Enumerations.Skills.Persuasion,
                Name = "Persuasion",
                Skill = Enumerations.Skills.Persuasion,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Religion",
                Id = (int)Enumerations.Skills.Religion,
                Name = "Religion",
                Skill = Enumerations.Skills.Religion,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "SleightOfHand",
                Id = (int)Enumerations.Skills.SleightOfHand,
                Name = "SleightOfHand",
                Skill = Enumerations.Skills.SleightOfHand,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Stealth",
                Id = (int)Enumerations.Skills.Stealth,
                Name = "Stealth",
                Skill = Enumerations.Skills.Stealth,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Survival",
                Id = (int)Enumerations.Skills.Survival,
                Name = "Survival",
                Skill = Enumerations.Skills.Survival,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Saving Throws",
                Id = (int)Enumerations.Skills.StrSaves,
                Name = "Strength",
                Skill = Enumerations.Skills.StrSaves,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Saving Throws",
                Id = (int)Enumerations.Skills.DexSaves,
                Name = "Desxterity",
                Skill = Enumerations.Skills.DexSaves,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Saving Throws",
                Id = (int)Enumerations.Skills.ConSaves,
                Name = "Constitution",
                Skill = Enumerations.Skills.ConSaves,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Saving Throws",
                Id = (int)Enumerations.Skills.IntSaves,
                Name = "Intelligence",
                Skill = Enumerations.Skills.IntSaves,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Saving Throws",
                Id = (int)Enumerations.Skills.WisSaves,
                Name = "Wisdom",
                Skill = Enumerations.Skills.WisSaves,
                IsTrained = false
            });
            model.Skills.Add(new SkillModel()
            {
                Bonus = 0,
                Description = "Saving Throws",
                Id = (int)Enumerations.Skills.ChaSaves,
                Name = "Charisma",
                Skill = Enumerations.Skills.ChaSaves,
                IsTrained = false
            });
            model.Speeds = new List<SpeedModel>();
            model.SpellCasting = SpellCastingModel.CreateNew();
            model.TemporaryHitPoints = 0;
            model.Value = 0;
            model.Weapons = new List<WeaponModel>();
            model.ProficiencyBonus = 2;
            return model;
        }
        public override string ToString()
        {
            return string.Format("{0} [CR: {1}] - {2}", Name, ChallengeRating, CreatureType);
        }
    }
}
