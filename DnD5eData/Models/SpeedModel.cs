﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class SpeedModel : BaseModel
    {
        public Enumerations.MovementTypes Movement { get; set; }
        public int Speed { get; set; }
    }
}
