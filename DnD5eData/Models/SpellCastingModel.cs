﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DnD5eData.Models
{
    public class SpellCastingModel : BaseModel
    {
        public SpellSlotModel Cantrips { get; set; }
        public SpellSlotModel Level1 { get; set; }
        public SpellSlotModel Level2 { get; set; }
        public SpellSlotModel Level3 { get; set; }
        public SpellSlotModel Level4 { get; set; }
        public SpellSlotModel Level5 { get; set; }
        public SpellSlotModel Level6 { get; set; }
        public SpellSlotModel Level7 { get; set; }
        public SpellSlotModel Level8 { get; set; }
        public SpellSlotModel Level9 { get; set; }

        public static SpellCastingModel CreateNew()
        {
            SpellCastingModel model = new SpellCastingModel();
            model.Cantrips = SpellSlotModel.CreateNew();
            model.Level1 = SpellSlotModel.CreateNew();
            model.Level2 = SpellSlotModel.CreateNew();
            model.Level3 = SpellSlotModel.CreateNew();
            model.Level4 = SpellSlotModel.CreateNew();
            model.Level5 = SpellSlotModel.CreateNew();
            model.Level6 = SpellSlotModel.CreateNew();
            model.Level7 = SpellSlotModel.CreateNew();
            model.Level8 = SpellSlotModel.CreateNew();
            model.Level9 = SpellSlotModel.CreateNew();
            return model;
        }
    }
}
