﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class WeaponModel : BaseModel
    {
        public Enumerations.DamageTypes DamageType { get; set; }
        public Enumerations.Dice Die { get; set; }
        public int NumberOfDice { get; set; }
        public int AttackBonus { get; set; }
        public int DamageBonus { get; set; }
        public bool IsVersitile { get; set; }
        public bool IsTwoHanded { get; set; }
        public Enumerations.WeaponClasses WeaponClass { get; set; }
    }
}
