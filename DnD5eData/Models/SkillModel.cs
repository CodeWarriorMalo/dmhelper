﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class SkillModel : BaseModel
    {
        public Enumerations.Skills Skill { get; set; }
        public int Bonus { get; set; }
        public bool IsTrained { get; set; }
    }
}
