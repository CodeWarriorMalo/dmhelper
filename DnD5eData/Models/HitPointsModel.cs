﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DnD5eData.Tools;
namespace DnD5eData.Models
{
    public class HitPointsModel
    {
        public int NumberOfDice { get; set; }
        public Enumerations.Dice Die { get; set; }
        public int Bonus { get; set; }
    }
}
